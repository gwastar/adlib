/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include "btree.h"
#include "compiler.h"

// TODO fix bad performance when compiling with gcc
//      (gcc specializes the API functions for the info parameter but even with __attribute__((flatten)) on all
//       API functions or __attribute__((always_inline)) on the compare function, gcc will never inline the
//       compare function (info->cmp), which really hurts performance for btree_set_benchmark since the keys
//       are simple integers and the compare function is trivial) (PGO fixes this issue)
// TODO is get_lower/upper_bound useful or are the iterator variants good enough?
// TODO implement APIs with hints for optimized bulk operations?
//      (figure out why the initial implementation did not improve performance...)

static void *btree_node_item(struct _btree_node *node, unsigned int idx, const struct btree_info *info)
{
	return node->data + info->key_alignment_offset + idx * info->item_size;
}

void *_btree_debug_node_item(struct _btree_node *node, unsigned int idx, const struct btree_info *info)
{
	return btree_node_item(node, idx, info);
}

static struct _btree_node **btree_node_children(struct _btree_node *node, const struct btree_info *info)
{
	char *children = (char *)btree_node_item(node, info->max_items, info);
	children += info->children_alignment_offset;
	// assert((uintptr_t)children % sizeof(void *) == 0);
	return (struct _btree_node **)children;
}

static struct _btree_node *btree_node_get_child(struct _btree_node *node, unsigned int idx,
						const struct btree_info *info)
{
	return btree_node_children(node, info)[idx];
}

struct _btree_node *_btree_debug_node_get_child(struct _btree_node *node, unsigned int idx,
						const struct btree_info *info)
{
	return btree_node_get_child(node, idx, info);
}

static void btree_node_set_child(struct _btree_node *node, unsigned int idx, struct _btree_node *child,
				 const struct btree_info *info)
{
	btree_node_children(node, info)[idx] = child;
}

static void btree_node_copy_child(struct _btree_node *dest_node, unsigned int dest_idx,
				  struct _btree_node *src_node, unsigned int src_idx,
				  const struct btree_info *info)
{
	btree_node_set_child(dest_node, dest_idx, btree_node_get_child(src_node, src_idx, info), info);
}

static void btree_node_get_item(void *item, struct _btree_node *node, unsigned int idx,
				const struct btree_info *info)
{
	memcpy(item, btree_node_item(node, idx, info), info->item_size);
}

static void btree_node_set_item(struct _btree_node *node, unsigned int idx, const void *item,
				const struct btree_info *info)
{
	memcpy(btree_node_item(node, idx, info), item, info->item_size);
}

static void btree_node_copy_item(struct _btree_node *dest_node, unsigned int dest_idx,
				 struct _btree_node *src_node, unsigned int src_idx,
				 const struct btree_info *info)
{
	btree_node_set_item(dest_node, dest_idx, btree_node_item(src_node, src_idx, info), info);
}

static bool btree_node_search(struct _btree_node *node, const void *key, unsigned int *ret_idx,
			      const struct btree_info *info)
{
	unsigned int start = 0;
	unsigned int end = node->num_items;
	while (start + info->linear_search_threshold < end) {
		unsigned int mid = (start + end) / 2; // start + end should never overflow
		int cmp = info->cmp(key, btree_node_item(node, mid, info));
		if (cmp == 0) {
			*ret_idx = mid;
			return true;
		} else if (cmp > 0) {
			start = mid + 1;
		} else {
			end = mid;
		}
	}

	while (start < end) {
		int cmp = info->cmp(key, btree_node_item(node, start, info));
		if (cmp == 0) {
			*ret_idx = start;
			return true;
		}
		if (cmp < 0) {
			break;
		}
		start++;
		end--;
		cmp = info->cmp(key, btree_node_item(node, end, info));
		if (cmp == 0) {
			*ret_idx = end;
			return true;
		}
		if (cmp > 0) {
			start = end + 1;
			break;
		}
	}
	*ret_idx = start;
	return false;
}

void *_btree_iter_start(struct _btree_iter *iter, const struct _btree *tree, bool rightmost,
			const struct btree_info *info)
{
	iter->tree = tree;
	iter->level = -1;

	if (tree->height == 0) {
		return NULL;
	}

	struct _btree_node *node = tree->root;
	struct _btree_pos *pos;
	for (unsigned int level = tree->height; level-- > 0;) {
		pos = &iter->path[level];
		pos->idx = rightmost ? node->num_items : 0;
		pos->node = node;
		node = level != 0 ? btree_node_get_child(node, pos->idx, info) : NULL;
	}
	iter->level = 0;
	if (rightmost) {
		pos->idx--;
	}
	return btree_node_item(pos->node, pos->idx, info);
}

void *_btree_iter_next(struct _btree_iter *iter, const struct btree_info *info)
{
	if (iter->level >= iter->tree->height) {
		return NULL;
	}
	struct _btree_pos *pos = &iter->path[iter->level];
	pos->idx++;
	// descend into the leftmost child of the right child
	while (iter->level != 0) {
		struct _btree_node *child = btree_node_get_child(pos->node, pos->idx, info);
		pos = &iter->path[--iter->level];
		pos->node = child;
		pos->idx = 0;
	}
	while (pos->idx >= pos->node->num_items) {
		if (++iter->level == iter->tree->height) {
			return NULL;
		}
		pos = &iter->path[iter->level];
	}
	return btree_node_item(pos->node, pos->idx, info);
}

void *_btree_iter_prev(struct _btree_iter *iter, const struct btree_info *info)
{
	if (iter->level >= iter->tree->height) {
		return NULL;
	}
	struct _btree_pos *pos = &iter->path[iter->level];
	// descend into the rightmost child of the left child
	while (iter->level != 0) {
		struct _btree_node *child = btree_node_get_child(pos->node, pos->idx, info);
		pos = &iter->path[--iter->level];
		pos->node = child;
		pos->idx = child->num_items;
	}
	while (pos->idx == 0) {
		if (++iter->level == iter->tree->height) {
			return NULL;
		}
		pos = &iter->path[iter->level];
	}
	pos->idx--;
	return btree_node_item(pos->node, pos->idx, info);
}

// TODO could unify this with _btree_iter_start?
void *_btree_iter_start_at(struct _btree_iter *iter, const struct _btree *tree, void *key,
			   enum btree_iter_start_at_mode mode, const struct btree_info *info)
{
	iter->tree = tree;
	iter->level = tree->height - 1;

	if (tree->height == 0) {
		return NULL;
	}

	struct _btree_node *node = tree->root;
	struct _btree_pos *pos;
	for (;;) {
		pos = &iter->path[iter->level];
		pos->node = node;
		if (btree_node_search(node, key, &pos->idx, info)) {
			if (mode == BTREE_ITER_LOWER_BOUND_EXCLUSIVE) {
				return _btree_iter_next(iter, info);
			} else if (mode == BTREE_ITER_UPPER_BOUND_EXCLUSIVE) {
				return _btree_iter_prev(iter, info);
			}
			break;
		}
		if (iter->level == 0) {
			switch (mode) {
			case BTREE_ITER_FIND_KEY:
				return NULL;
			case BTREE_ITER_LOWER_BOUND_INCLUSIVE:
			case BTREE_ITER_LOWER_BOUND_EXCLUSIVE:
				if (pos->idx == pos->node->num_items) {
					return _btree_iter_next(iter, info);
				}
				break;
			case BTREE_ITER_UPPER_BOUND_INCLUSIVE:
			case BTREE_ITER_UPPER_BOUND_EXCLUSIVE:
				return _btree_iter_prev(iter, info);
			}
			break;
		}
		node = btree_node_get_child(node, pos->idx, info);
		iter->level--;
	}
	return btree_node_item(pos->node, pos->idx, info);
}

static size_t btree_node_size(bool leaf, const struct btree_info *info)
{
	size_t size = sizeof(struct _btree_node);
	size += info->key_alignment_offset;
	size += info->max_items * info->item_size;
	if (!leaf) {
		size += info->children_alignment_offset;
		size += (info->max_items + 1) * sizeof(struct _btree_node *);
	}
	return size;
}

static struct _btree_node *btree_new_node(bool leaf, const struct btree_info *info)
{
	struct _btree_node *node;
	size_t size = btree_node_size(leaf, info);
	node = malloc(size);
	node->num_items = 0;
	return node;
}

void _btree_init(struct _btree *tree)
{
	memset(tree, 0, sizeof(*tree));
}

void _btree_destroy(struct _btree *tree, const struct btree_info *info)
{
	if (tree->height == 0) {
		return;
	}
	struct _btree_pos path[__BTREE_MAX_HEIGHT];
	struct _btree_node *node = tree->root;
	for (unsigned int level = tree->height; level-- > 0;) {
		path[level].idx = 0;
		path[level].node = node;
		node = level != 0 ? btree_node_get_child(node, 0, info) : NULL;
	}

	unsigned int level = 0;
	for (;;) {
		// start at leaf
		struct _btree_pos *pos = &path[level];
		// free the leaf and go up, if we are at the end of the current node free it and keep going up
		do {
			if (info->destroy_item) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_item(btree_node_item(pos->node, i, info));
				}
			}
			free(pos->node);
			if (++level == tree->height) {
				tree->root = NULL;
				tree->height = 0;
				return;
			}
			pos = &path[level];
		} while (pos->idx >= pos->node->num_items);
		// descend into the leftmost child of the right child
		pos->idx++;
		do {
			struct _btree_node *child = btree_node_get_child(pos->node, pos->idx, info);
			pos = &path[--level];
			pos->node = child;
			pos->idx = 0;
		} while (level != 0);
	}
}

void *_btree_find(const struct _btree *tree, const void *key, const struct btree_info *info)
{
	if (tree->height == 0) {
		return NULL;
	}
	struct _btree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	for (;;) {
		unsigned int idx;
		if (btree_node_search(node, key, &idx, info)) {
			return btree_node_item(node, idx, info);
		}
		if (level-- == 0) {
			break;
		}
		node = btree_node_get_child(node, idx, info);
	}
	return NULL;
}

void *_btree_get_leftmost_rightmost(const struct _btree *tree, bool leftmost, const struct btree_info *info)
{
	if (tree->height == 0) {
		return NULL;
	}
	struct _btree_node *node = tree->root;
	unsigned int level = tree->height;
	while (--level != 0) {
		unsigned int idx = leftmost ? 0 : node->num_items;
		node = btree_node_get_child(node, idx, info);
	}
	unsigned int idx = leftmost ? 0 : node->num_items - 1;
	return btree_node_item(node, idx, info);
}

static void btree_node_shift_items_right(struct _btree_node *node, unsigned int idx,
					 const struct btree_info *info)
{
	memmove(btree_node_item(node, idx + 1, info),
		btree_node_item(node, idx, info),
		(node->num_items - idx) * info->item_size);
}

static void btree_node_shift_children_right(struct _btree_node *node, unsigned int idx,
					    const struct btree_info *info)
{
	memmove(btree_node_children(node, info) + idx + 1,
		btree_node_children(node, info) + idx,
		(node->num_items + 1 - idx) * sizeof(struct _btree_node *));
}

static void btree_node_shift_items_left(struct _btree_node *node, unsigned int idx,
					const struct btree_info *info)
{
	memmove(btree_node_item(node, idx, info),
		btree_node_item(node, idx + 1, info),
		(node->num_items - idx - 1) * info->item_size);
}

static void btree_node_shift_children_left(struct _btree_node *node, unsigned int idx,
					   const struct btree_info *info)
{
	memmove(btree_node_children(node, info) + idx,
		btree_node_children(node, info) + idx + 1,
		(node->num_items - idx) * sizeof(struct _btree_node *));
}

bool _btree_delete(struct _btree *tree, enum _btree_deletion_mode mode, const void *key, void *ret_item,
		   const struct btree_info *info)
{
	if (tree->height == 0) {
		return false;
	}
	struct _btree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	struct _btree_pos path[__BTREE_MAX_HEIGHT];
	unsigned int idx;
	bool leaf = false;
	for (;;) {
		leaf = level == 0;
		bool found = false;
		switch (mode) {
		case __BTREE_DELETE_KEY:
			found = btree_node_search(node, key, &idx, info);
			if (leaf && !found) {
				return false;
			}
			break;
		case __BTREE_DELETE_MIN:
			idx = 0;
			break;
		case __BTREE_DELETE_MAX:
			idx = leaf ? node->num_items - 1 : node->num_items;
			break;
		}
		// TODO maybe move most of this code into the switch
		if (leaf) {
			break;
		}
		if (found) {
			// we found the key in an internal node
			// now return it and then replace it with the max value of the left subtree
			if (ret_item) {
				btree_node_get_item(ret_item, node, idx, info);
			} else if (info->destroy_item) {
				info->destroy_item(btree_node_item(node, idx, info));
			}
			ret_item = btree_node_item(node, idx, info);
			mode = __BTREE_DELETE_MAX;
		}
		path[level].idx = idx;
		path[level].node = node;
		level--;
		node = btree_node_get_child(node, idx, info);
	}

	// we are at the leaf and have found the item to delete
	// return the item and rebalance

	if (ret_item) {
		btree_node_get_item(ret_item, node, idx, info);
	} else if (info->destroy_item) {
		info->destroy_item(btree_node_item(node, idx, info));
	}
	btree_node_shift_items_left(node, idx, info);
	// assert(node->num_items != 0);
	node->num_items--;

	while (++level != tree->height) {
		if (node->num_items >= info->min_items) {
			return true;
		}

		node = path[level].node;
		idx = path[level].idx;

		if (idx == node->num_items) {
			idx--;
		}
		struct _btree_node *left = btree_node_get_child(node, idx, info);
		struct _btree_node *right = btree_node_get_child(node, idx + 1, info);

		if (left->num_items + right->num_items < info->max_items) {
			btree_node_copy_item(left, left->num_items, node, idx, info);
			btree_node_shift_items_left(node, idx, info);
			btree_node_shift_children_left(node, idx + 1, info);
			node->num_items--;
			left->num_items++;
			memcpy(btree_node_item(left, left->num_items, info),
			       btree_node_item(right, 0, info),
			       right->num_items * info->item_size);
			if (!leaf) {
				memcpy(btree_node_children(left, info) + left->num_items,
				       btree_node_children(right, info),
				       (right->num_items + 1) * sizeof(struct _btree_node *));
			}
			left->num_items += right->num_items;
			free(right);
		} else if (left->num_items > right->num_items) {
			btree_node_shift_items_right(right, 0, info);
			btree_node_copy_item(right, 0, node, idx, info);
			btree_node_copy_item(node, idx, left, left->num_items - 1, info);
			if (!leaf) {
				btree_node_shift_children_right(right, 0, info);
				btree_node_copy_child(right, 0, left, left->num_items, info);
			}
			left->num_items--;
			right->num_items++;
		} else {
			btree_node_copy_item(left, left->num_items, node, idx, info);
			btree_node_copy_item(node, idx, right, 0, info);
			btree_node_shift_items_left(right, 0, info);
			if (!leaf) {
				btree_node_copy_child(left, left->num_items + 1, right, 0, info);
				btree_node_shift_children_left(right, 0, info);
			}
			right->num_items--;
			left->num_items++;
		}

		leaf = false;
	}

	// assert(node == tree->root);

	if (node->num_items == 0) {
		tree->root = NULL;
		if (!leaf) {
			tree->root = btree_node_get_child(node, 0, info);
		}
		tree->height--;
		free(node);
	}

	return true;
}

/* item will be inserted and then set to the median */
static struct _btree_node *btree_node_split_and_insert(struct _btree_node *node, unsigned int idx,
						       void **item, struct _btree_node *right,
						       bool bulk_loading, const struct btree_info *info)
{
	// assert(node->num_items == info->max_items);
	struct _btree_node *new_node = btree_new_node(!right, info);
	if (bulk_loading) {
		// assert(idx == info->max_items);
		if (right) {
			btree_node_set_child(new_node, 0, right, info);
		}
		return new_node;
	}
	node->num_items = info->min_items;
	new_node->num_items = info->max_items - info->min_items;

	if (idx < node->num_items) {
		memcpy(btree_node_item(new_node, 0, info),
		       btree_node_item(node, node->num_items, info),
		       new_node->num_items * info->item_size);
		btree_node_shift_items_right(node, idx, info);
		btree_node_set_item(node, idx, *item, info);

		// the median got shifted right by one
		*item = btree_node_item(node, node->num_items, info); // return median

		if (right) {
			memcpy(btree_node_children(new_node, info),
			       btree_node_children(node, info) + node->num_items,
			       (new_node->num_items + 1) * sizeof(struct _btree_node *));
			btree_node_shift_children_right(node, idx + 1, info);
			btree_node_set_child(node, idx + 1, right, info);
		}
	} else if (idx == node->num_items) {
		// item is already set to median
		memcpy(btree_node_item(new_node, 0, info),
		       btree_node_item(node, node->num_items, info),
		       new_node->num_items * info->item_size);

		if (right) {
			memcpy(btree_node_children(new_node, info) + 1,
			       btree_node_children(node, info) + node->num_items + 1,
			       new_node->num_items * sizeof(struct _btree_node *));
			btree_node_set_child(new_node, 0, right, info);
		}
	} else {
		idx -= node->num_items + 1;
		// it is not worth splitting the memcpys here in two to avoid the shifts
		memcpy(btree_node_item(new_node, 0, info),
		       btree_node_item(node, node->num_items + 1, info),
		       (new_node->num_items - 1) * info->item_size);
		new_node->num_items--; // Hack: we have only inserted new_node->num_items - 1 items so far
		btree_node_shift_items_right(new_node, idx, info);
		new_node->num_items++;
		btree_node_set_item(new_node, idx, *item, info);

		*item = btree_node_item(node, node->num_items, info); // return median

		if (right) {
			memcpy(btree_node_children(new_node, info),
			       btree_node_children(node, info) + node->num_items + 1,
			       new_node->num_items * sizeof(struct _btree_node *));
			new_node->num_items--; // Hack
			btree_node_shift_children_right(new_node, idx + 1, info);
			new_node->num_items++;
			btree_node_set_child(new_node, idx + 1, right, info);
		}
	}

	return new_node;
}

static void btree_insert_and_rebalance(struct _btree *tree, void *item,
				       struct _btree_pos path[static __BTREE_MAX_HEIGHT],
				       bool bulk_loading, const struct btree_info *info)
{
	unsigned int level = 0;
	struct _btree_node *node;
	struct _btree_node *right = NULL;
	do {
		unsigned int idx = path[level].idx++;
		node = path[level].node;

		if (node->num_items < info->max_items) {
			btree_node_shift_items_right(node, idx, info);
			btree_node_set_item(node, idx, item, info);
			if (right) {
				btree_node_shift_children_right(node, idx + 1, info);
				btree_node_set_child(node, idx + 1, right, info);
			}
			node->num_items++;
			return;
		}

		right = btree_node_split_and_insert(node, idx, &item, right, bulk_loading, info);
		path[level].node = right;
		path[level].idx = right->num_items;
	} while (++level != tree->height);
	struct _btree_node *new_root = btree_new_node(false, info);
	btree_node_set_item(new_root, 0, item, info);
	new_root->num_items = 1;
	btree_node_set_child(new_root, 0, node, info);
	btree_node_set_child(new_root, 1, right, info);
	tree->root = new_root;
	tree->height++;
	path[level].idx = 1;
	path[level].node = tree->root;
}

bool _btree_insert(struct _btree *tree, void *item, bool update, const struct btree_info *info)
{
	if (tree->height == 0) {
		tree->root = btree_new_node(true, info);
		tree->height = 1;
	}
	struct _btree_node *node = tree->root;
	struct _btree_pos path[__BTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		if (btree_node_search(node, item, &idx, info)) {
			if (update) {
				if (info->destroy_item) {
					info->destroy_item(btree_node_item(node, idx, info));
				}
				btree_node_set_item(node, idx, item, info);
			}
			return false;
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = btree_node_get_child(node, idx, info);
	}
	btree_insert_and_rebalance(tree, item, path, false, info);
	return true;
}

bool _btree_insert_sequential(struct _btree *tree, void *item, const struct btree_info *info)
{
	if (tree->height == 0) {
		return _btree_insert(tree, item, false, info);
	}
	struct _btree_node *node = tree->root;
	struct _btree_pos path[__BTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		idx = node->num_items;
		if (unlikely(info->cmp(item, btree_node_item(node, idx - 1, info)) <= 0)) {
			return _btree_insert(tree, item, false, info);
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = btree_node_get_child(node, idx, info);
	}
	btree_insert_and_rebalance(tree, item, path, false, info);
	return true;
}

void _btree_bulk_load_start(struct _btree_bulk_load_ctx *ctx)
{
	_btree_init(&ctx->tree);
}

void _btree_bulk_load_next(struct _btree_bulk_load_ctx *ctx, void *item, const struct btree_info *info)
{
	if (ctx->tree.height == 0) {
		struct _btree_node *root = btree_new_node(true, info);
		ctx->tree.root = root;
		ctx->tree.height = 1;
		ctx->path[0].node = root;
		ctx->path[0].idx = 0;
	}
	btree_insert_and_rebalance(&ctx->tree, item, ctx->path, true, info);
}

void _btree_bulk_load_end(struct _btree_bulk_load_ctx *ctx, const struct btree_info *info)
{
	if (ctx->tree.height == 0) {
		return;
	}
	struct _btree_node *parent = ctx->tree.root;
	for (unsigned int level = ctx->tree.height - 1; level-- > 0;) {
		struct _btree_node *node = ctx->path[level].node;
		if (node->num_items >= info->min_items) {
			parent = node;
			continue;
		}
		unsigned int idx_in_parent = parent->num_items;
		// assert(idx_in_parent != 0);
		struct _btree_node *left = btree_node_get_child(parent, idx_in_parent - 1, info);
		// assert(left->num_items == info->max_items);
		unsigned int n = info->min_items - node->num_items;
		memmove(btree_node_item(node, n, info),
			btree_node_item(node, 0, info),
			node->num_items * info->item_size);
		n--;
		btree_node_copy_item(node, n, parent, idx_in_parent - 1, info);
		memcpy(btree_node_item(node, 0, info),
		       btree_node_item(left, left->num_items - n, info),
		       n * info->item_size);
		n = left->num_items - n - 1;
		btree_node_copy_item(parent, idx_in_parent - 1, left, n, info);
		if (level != 0) {
			unsigned int n = info->min_items - node->num_items;
			memmove(btree_node_children(node, info) + n,
				btree_node_children(node, info),
				(node->num_items + 1) * sizeof(struct _btree_node *));
			memcpy(btree_node_children(node, info),
			       btree_node_children(left, info) + left->num_items + 1 - n,
			       n * sizeof(struct _btree_node *));
		}
		left->num_items -= info->min_items - node->num_items;
		node->num_items = info->min_items;
		parent = node;
	}
}

static struct _btree_node *btree_node_copy(struct _btree_node *node, unsigned int level,
					   const struct btree_info *info)
{
	struct _btree_node *copy = btree_new_node(level == 0, info);
	memcpy(btree_node_item(copy, 0, info),
	       btree_node_item(node, 0, info),
	       node->num_items * info->item_size);
	copy->num_items = node->num_items;
	if (level == 0) {
		return copy;
	}
	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		struct _btree_node *child = btree_node_get_child(node, i, info);
		struct _btree_node *child_copy = btree_node_copy(child, level - 1, info);
		btree_node_set_child(copy, i, child_copy, info);
	}
	return copy;
}

struct _btree _btree_debug_copy(const struct _btree *tree, const struct btree_info *info)
{
	unsigned int height = tree->height;
	struct _btree copy;
	copy.height = height;
	copy.root = height == 0 ? NULL : btree_node_copy(tree->root, height - 1, info);
	return copy;
}

size_t _btree_debug_node_size(bool leaf, bool root, const struct btree_info *info)
{
	(void)root;
	return btree_node_size(leaf, info);
}
