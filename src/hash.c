/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <string.h>
#include "hash.h"
#include "mx3.h"
#include "uint128.h"

/*
  SipHash reference C implementation
  Copyright (c) 2012-2021 Jean-Philippe Aumasson
  <jeanphilippe.aumasson@gmail.com>
  Copyright (c) 2012-2014 Daniel J. Bernstein <djb@cr.yp.to>
  To the extent possible under law, the author(s) have dedicated all copyright
  and related and neighboring rights to this software to the public domain
  worldwide. This software is distributed without any warranty.
  You should have received a copy of the CC0 Public Domain Dedication along
  with
  this software. If not, see
  <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

#define ROTL64(x, b) (uint64_t)(((x) << (b)) | ((x) >> (64 - (b))))

#define U64TO8_LE(p, v)					\
	(p)[0] = (uint8_t)((v));			\
	(p)[1] = (uint8_t)((v) >> 8);			\
	(p)[2] = (uint8_t)((v) >> 16);			\
	(p)[3] = (uint8_t)((v) >> 24);			\
	(p)[4] = (uint8_t)((v) >> 32);			\
	(p)[5] = (uint8_t)((v) >> 40);			\
	(p)[6] = (uint8_t)((v) >> 48);			\
	(p)[7] = (uint8_t)((v) >> 56);			\

#define U8TO64_LE(p)							\
	(((uint64_t)((p)[0])) | ((uint64_t)((p)[1]) << 8) |		\
	 ((uint64_t)((p)[2]) << 16) | ((uint64_t)((p)[3]) << 24) |	\
	 ((uint64_t)((p)[4]) << 32) | ((uint64_t)((p)[5]) << 40) |	\
	 ((uint64_t)((p)[6]) << 48) | ((uint64_t)((p)[7]) << 56))

#define SIPROUND				\
	do {					\
		v0 += v1;			\
		v1 = ROTL64(v1, 13);		\
		v1 ^= v0;			\
		v0 = ROTL64(v0, 32);		\
		v2 += v3;			\
		v3 = ROTL64(v3, 16);		\
		v3 ^= v2;			\
		v0 += v3;			\
		v3 = ROTL64(v3, 21);		\
		v3 ^= v0;			\
		v2 += v1;			\
		v1 = ROTL64(v1, 17);		\
		v1 ^= v2;			\
		v2 = ROTL64(v2, 32);		\
	} while (0)

static _attr_always_inline void siphash(const void *in, const size_t inlen, const void *k,
					unsigned char *out, const size_t outlen,
					const int cROUNDS, const int dROUNDS)
{
	const unsigned char *ni = (const unsigned char *)in;
	const unsigned char *kk = (const unsigned char *)k;

	// assert((outlen == 8) || (outlen == 16));
	uint64_t v0 = UINT64_C(0x736f6d6570736575);
	uint64_t v1 = UINT64_C(0x646f72616e646f6d);
	uint64_t v2 = UINT64_C(0x6c7967656e657261);
	uint64_t v3 = UINT64_C(0x7465646279746573);
	uint64_t k0 = U8TO64_LE(kk);
	uint64_t k1 = U8TO64_LE(kk + 8);
	uint64_t m;
	int i;
	const unsigned char *end = ni + inlen - (inlen % sizeof(uint64_t));
	const int left = inlen & 7;
	uint64_t b = ((uint64_t)inlen) << 56;
	v3 ^= k1;
	v2 ^= k0;
	v1 ^= k1;
	v0 ^= k0;

	if (outlen == 16) {
		v1 ^= 0xee;
	}

	for (; ni != end; ni += 8) {
		m = U8TO64_LE(ni);
		v3 ^= m;

		for (i = 0; i < cROUNDS; ++i) {
			SIPROUND;
		}

		v0 ^= m;
	}

	switch (left) {
	case 7:
		b |= ((uint64_t)ni[6]) << 48; _attr_fallthrough;
	case 6:
		b |= ((uint64_t)ni[5]) << 40; _attr_fallthrough;
	case 5:
		b |= ((uint64_t)ni[4]) << 32; _attr_fallthrough;
	case 4:
		b |= ((uint64_t)ni[3]) << 24; _attr_fallthrough;
	case 3:
		b |= ((uint64_t)ni[2]) << 16; _attr_fallthrough;
	case 2:
		b |= ((uint64_t)ni[1]) << 8; _attr_fallthrough;
	case 1:
		b |= ((uint64_t)ni[0]); _attr_fallthrough;
	case 0:
		break;
	}

	v3 ^= b;

	for (i = 0; i < cROUNDS; ++i) {
		SIPROUND;
	}

	v0 ^= b;

	if (outlen == 16) {
		v2 ^= 0xee;
	} else {
		v2 ^= 0xff;
	}

	for (i = 0; i < dROUNDS; ++i) {
		SIPROUND;
	}

	b = v0 ^ v1 ^ v2 ^ v3;
	U64TO8_LE(out, b);

	if (outlen == 8) {
		return;
	}

	v1 ^= 0xdd;

	for (i = 0; i < dROUNDS; ++i) {
		SIPROUND;
	}

	b = v0 ^ v1 ^ v2 ^ v3;
	U64TO8_LE(out + 8, b);
}

hash64_t siphash24_64(const void *buf, size_t buflen, const unsigned char key[static 16])
{
	hash64_t out;
	siphash(buf, buflen, key, out.bytes, sizeof(out), 2, 4);
	return out;
}

hash128_t siphash24_128(const void *buf, size_t buflen, const unsigned char key[static 16])
{
	hash128_t out;
	siphash(buf, buflen, key, out.bytes, sizeof(out), 2, 4);
	return out;
}

hash64_t siphash13_64(const void *buf, size_t buflen, const unsigned char key[static 16])
{
	hash64_t out;
	siphash(buf, buflen, key, out.bytes, sizeof(out), 1, 3);
	return out;
}

hash128_t siphash13_128(const void *buf, size_t buflen, const unsigned char key[static 16])
{
	hash128_t out;
	siphash(buf, buflen, key, out.bytes, sizeof(out), 1, 3);
	return out;
}

uint64_t mx3_hash(const void *buf, size_t len, uint64_t seed)
{
	return mx3_hash_impl(buf, len, seed);
}

uint32_t int_hash32(uint32_t v)
{
	// https://github.com/skeeto/hash-prospector/issues/19#issuecomment-1120105785
	v ^= v >> 16;
	v *= UINT32_C(0x21f0aaad);
	v ^= v >> 15;
	v *= UINT32_C(0x735a2d97);
	v ^= v >> 15;
	return v;
}

uint64_t int_hash64(uint64_t v)
{
	return mx3_mix(v);
}

uint32_t fibonacci_hash32(uint32_t val, unsigned int bits)
{
	// val *= UINT32_C(2654435769);
	val *= UINT32_C(1640531527);
	return val >> (32 - bits);
}

uint64_t fibonacci_hash64(uint64_t val, unsigned int bits)
{
	// val *= UINT64_C(11400714819323198485);
	val *= UINT64_C(7046029254386353131);
	return val >> (64 - bits);
}

uint32_t pair_hash_int32(uint32_t val1, uint32_t val2)
{
	uint64_t m = (uint64_t)val1 * (uint64_t)(val2 ^ UINT32_C(1640531527));
	return int_hash32(m ^ (m >> 32));
}

uint64_t pair_hash_int64(uint64_t val1, uint64_t val2)
{
	// return int_hash64(val1 ^ ROTL64(val1, 32) ^ (UINT64_C(11400714819323198485) * val2));
	uint128_t m = uint128_mul64(val1, val2 ^ UINT64_C(11400714819323198485));
	return int_hash64(uint128_get_low_bits(m) ^ uint128_get_high_bits(m));
}
