/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "bplusstartree.h"
#include "compiler.h"

#define BPSTARTREE_MAX_HEIGHT 48

struct _bpstartree_pos {
	struct _bpstartree_node *node;
	unsigned int idx;
};

static unsigned int bpstartree_root_max_items(const struct bplusstartree_info *info)
{
	return 2 * info->min_items;
}

static void *bpstartree_node_key(struct _bpstartree_node *node, unsigned int idx,
				 const struct bplusstartree_info *info)
{
	return node->data + info->key_alignment_offset + idx * info->key_size;
}

static void *bpstartree_node_value(struct _bpstartree_node *node, unsigned int idx,
				   const struct bplusstartree_info *info)
{
	unsigned int max_items = node->root ? bpstartree_root_max_items(info) : info->max_items;
	unsigned int value_alignment_offset = node->root ? info->root_value_alignment_offset : info->value_alignment_offset;
	return node->data + info->key_alignment_offset + max_items * info->key_size +
		value_alignment_offset + idx * info->value_size;
}

static struct _bpstartree_leaf_link *bpstartree_leaf_link(struct _bpstartree_node *node,
							  const struct bplusstartree_info *info)
{
	(void)info;
	return (struct _bpstartree_leaf_link *)node - 1;
}

static struct _bpstartree_node *bpstartree_leaf_link_to_node(struct _bpstartree_leaf_link *link,
							     const struct bplusstartree_info *info)
{
	(void)info;
	return (struct _bpstartree_node *)(link + 1);
}

static struct _bpstartree_node **bpstartree_node_children(struct _bpstartree_node *node,
							  const struct bplusstartree_info *info)
{
	char *children = (char *)bpstartree_node_value(node, 0, info);
	// assert((uintptr_t)children % sizeof(void *) == 0);
	return (struct _bpstartree_node **)children;
}

static struct _bpstartree_node *bpstartree_node_get_child(struct _bpstartree_node *node, unsigned int idx,
							  const struct bplusstartree_info *info)
{
	return bpstartree_node_children(node, info)[idx];
}

static void bpstartree_node_set_child(struct _bpstartree_node *node, unsigned int idx,
				      struct _bpstartree_node *child, const struct bplusstartree_info *info)
{
	bpstartree_node_children(node, info)[idx] = child;
}

static void bpstartree_node_copy_child(struct _bpstartree_node *dest_node, unsigned int dest_idx,
				       struct _bpstartree_node *src_node, unsigned int src_idx,
				       const struct bplusstartree_info *info)
{
	bpstartree_node_set_child(dest_node, dest_idx, bpstartree_node_get_child(src_node, src_idx, info), info);
}

static void bpstartree_node_copy_children(struct _bpstartree_node *dest_node, unsigned int dest_idx,
					  struct _bpstartree_node *src_node, unsigned int src_idx,
					  unsigned int num_children, const struct bplusstartree_info *info)
{
	memcpy(bpstartree_node_children(dest_node, info) + dest_idx,
	       bpstartree_node_children(src_node, info) + src_idx,
	       num_children * sizeof(struct _bpstartree_node *));
}

static void bpstartree_node_get_key(void *key, struct _bpstartree_node *node, unsigned int idx,
				    const struct bplusstartree_info *info)
{
	memcpy(key, bpstartree_node_key(node, idx, info), info->key_size);
}

static void bpstartree_node_set_key(struct _bpstartree_node *node, unsigned int idx, const void *key,
				    const struct bplusstartree_info *info)
{
	memcpy(bpstartree_node_key(node, idx, info), key, info->key_size);
}

static void bpstartree_node_copy_key(struct _bpstartree_node *dest_node, unsigned int dest_idx,
				     struct _bpstartree_node *src_node, unsigned int src_idx,
				     const struct bplusstartree_info *info)
{
	bpstartree_node_set_key(dest_node, dest_idx, bpstartree_node_key(src_node, src_idx, info), info);
}

static void bpstartree_node_copy_keys(struct _bpstartree_node *dest_node, unsigned int dest_idx,
				      struct _bpstartree_node *src_node, unsigned int src_idx,
				      unsigned int num_keys, const struct bplusstartree_info *info)
{
	memcpy(bpstartree_node_key(dest_node, dest_idx, info),
	       bpstartree_node_key(src_node, src_idx, info),
	       num_keys * info->key_size);
}

static void bpstartree_node_get_value(void *value, struct _bpstartree_node *node, unsigned int idx,
				      const struct bplusstartree_info *info)
{
	memcpy(value, bpstartree_node_value(node, idx, info), info->value_size);
}

static void bpstartree_node_set_value(struct _bpstartree_node *node, unsigned int idx, const void *value,
				      const struct bplusstartree_info *info)
{
	memcpy(bpstartree_node_value(node, idx, info), value, info->value_size);
}

static void bpstartree_node_copy_value(struct _bpstartree_node *dest_node, unsigned int dest_idx,
				       struct _bpstartree_node *src_node, unsigned int src_idx,
				       const struct bplusstartree_info *info)
{
	bpstartree_node_set_value(dest_node, dest_idx, bpstartree_node_value(src_node, src_idx, info), info);
}

static void bpstartree_node_copy_values(struct _bpstartree_node *dest_node, unsigned int dest_idx,
					struct _bpstartree_node *src_node, unsigned int src_idx,
					unsigned int num_values, const struct bplusstartree_info *info)
{
	memcpy(bpstartree_node_value(dest_node, dest_idx, info),
	       bpstartree_node_value(src_node, src_idx, info),
	       num_values * info->value_size);
}

static bool bpstartree_node_search(struct _bpstartree_node *node, const void *key, unsigned int *ret_idx,
				   const struct bplusstartree_info *info)
{
	unsigned int start = 0;
	unsigned int end = node->num_items;
	while (start + info->linear_search_threshold < end) {
		unsigned int mid = (start + end) / 2; // start + end should never overflow
		int cmp = info->cmp(key, bpstartree_node_key(node, mid, info));
		if (cmp == 0) {
			*ret_idx = mid;
			return true;
		} else if (cmp > 0) {
			start = mid + 1;
		} else {
			end = mid;
		}
	}

	while (start < end) {
		int cmp = info->cmp(key, bpstartree_node_key(node, start, info));
		if (cmp == 0) {
			*ret_idx = start;
			return true;
		}
		if (cmp < 0) {
			break;
		}
		start++;
		end--;
		cmp = info->cmp(key, bpstartree_node_key(node, end, info));
		if (cmp == 0) {
			*ret_idx = end;
			return true;
		}
		if (cmp > 0) {
			start = end + 1;
			break;
		}
	}
	*ret_idx = start;
	return false;
}

_bpstartree_kv_t _bpstartree_iter_start(struct _bpstartree_iter *iter, const struct _bpstartree *tree,
					bool rightmost, const struct bplusstartree_info *info)
{
	iter->tree = tree;

	if (tree->height == 0) {
		iter->node = NULL;
		iter->idx = 0;
		return (_bpstartree_kv_t) { NULL, NULL };
	}

	iter->node = bpstartree_leaf_link_to_node(rightmost ? tree->list->prev : tree->list->next, info);
	iter->idx = rightmost ? iter->node->num_items - 1 : 0;
	return (_bpstartree_kv_t) {
		bpstartree_node_key(iter->node, iter->idx, info),
		bpstartree_node_value(iter->node, iter->idx, info),
	};
}

_bpstartree_kv_t _bpstartree_iter_next(struct _bpstartree_iter *iter, const struct bplusstartree_info *info)
{
	if (!iter->node) {
		return (_bpstartree_kv_t) { NULL, NULL };
	}
	iter->idx++;
	if (iter->idx >= iter->node->num_items) {
		iter->idx = 0;
		struct _bpstartree_leaf_link *next = bpstartree_leaf_link(iter->node, info)->next;
		if (next == iter->tree->list) {
			iter->node = NULL;
			return (_bpstartree_kv_t) { NULL, NULL };
		}
		iter->node = bpstartree_leaf_link_to_node(next, info);
	}
	return (_bpstartree_kv_t) {
		bpstartree_node_key(iter->node, iter->idx, info),
		bpstartree_node_value(iter->node, iter->idx, info),
	};
}

_bpstartree_kv_t _bpstartree_iter_prev(struct _bpstartree_iter *iter, const struct bplusstartree_info *info)
{
	if (!iter->node) {
		return (_bpstartree_kv_t) { NULL, NULL };
	}
	if (iter->idx == 0) {
		struct _bpstartree_leaf_link *prev = bpstartree_leaf_link(iter->node, info)->prev;
		if (prev == iter->tree->list) {
			iter->node = NULL;
			return (_bpstartree_kv_t) { NULL, NULL };
		}
		iter->node = bpstartree_leaf_link_to_node(prev, info);
		iter->idx = iter->node->num_items;
	}
	iter->idx--;
	return (_bpstartree_kv_t) {
		bpstartree_node_key(iter->node, iter->idx, info),
		bpstartree_node_value(iter->node, iter->idx, info),
	};
}

_bpstartree_kv_t _bpstartree_iter_start_at(struct _bpstartree_iter *iter, const struct _bpstartree *tree,
					   void *key, enum bplusstartree_iter_start_at_mode mode,
					   const struct bplusstartree_info *info)
{
	iter->tree = tree;

	if (tree->height == 0) {
		iter->node = NULL;
		iter->idx = 0;
		return (_bpstartree_kv_t) { NULL, NULL };
	}

	struct _bpstartree_node *node = tree->root;
	unsigned int idx;
	bool found;
	size_t level = tree->height;
	for (;;) {
		found = bpstartree_node_search(node, key, &idx, info);
		if (--level == 0) {
			break;
		}
		node = bpstartree_node_get_child(node, idx + found, info);
	}
	switch (mode) {
	case BPLUSSTARTREE_ITER_FIND_KEY:
		if (!found) {
			return (_bpstartree_kv_t) { NULL, NULL };
		}
		iter->node = node;
		iter->idx = idx;
		break;
	case BPLUSSTARTREE_ITER_LOWER_BOUND_EXCLUSIVE:
		idx += found;
		_attr_fallthrough;
	case BPLUSSTARTREE_ITER_LOWER_BOUND_INCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		if (idx == node->num_items) {
			return _bpstartree_iter_next(iter, info);
		}
		break;
	case BPLUSSTARTREE_ITER_UPPER_BOUND_EXCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		return _bpstartree_iter_prev(iter, info);
	case BPLUSSTARTREE_ITER_UPPER_BOUND_INCLUSIVE:
		iter->node = node;
		iter->idx = idx;
		if (!found) {
			return _bpstartree_iter_prev(iter, info);
		}
		break;
	}
	return (_bpstartree_kv_t) {
		bpstartree_node_key(iter->node, iter->idx, info),
		bpstartree_node_value(iter->node, iter->idx, info),
	};
}

static size_t bpstartree_node_size(bool leaf, bool root, const struct bplusstartree_info *info)
{
	size_t max_items = root ? bpstartree_root_max_items(info) : info->max_items;
	unsigned char value_alignment_offset = root ? info->root_value_alignment_offset : info->value_alignment_offset;
	size_t size = sizeof(struct _bpstartree_node);
	size += info->key_alignment_offset;
	size += max_items * info->key_size;
	size += value_alignment_offset;
	if (leaf) {
		size += max_items * info->value_size;
		size += sizeof(struct _bpstartree_leaf_link);
	} else {
		size += (max_items + 1) * sizeof(struct _bpstartree_node *);
	}
	return size;
}

static struct _bpstartree_node *bpstartree_new_node(bool leaf, bool root,
						    const struct bplusstartree_info *info)
{
	struct _bpstartree_node *node;
	size_t size = bpstartree_node_size(leaf, root, info);
	void *p = malloc(size);
	if (leaf) {
		node = bpstartree_leaf_link_to_node(p, info);
	} else {
		node = p;
	}
	node->num_items = 0;
	node->root = root;
	return node;
}

static void bpstartree_free_node(struct _bpstartree_node *node, bool leaf, const struct bplusstartree_info *info)
{
	if (leaf) {
		free(bpstartree_leaf_link(node, info));
	} else {
		free(node);
	}
}

// this function only preserves the node header!
static struct _bpstartree_node *bpstartree_realloc_root(struct _bpstartree *tree, bool leaf,
							const struct bplusstartree_info *info)
{
	// assert(tree->root->root);
	void *p = tree->root;
	unsigned int num_items = tree->root->num_items;
	if (!leaf) {
		// root was leaf
		p = bpstartree_leaf_link(tree->root, info);
	}
	size_t new_size = bpstartree_node_size(leaf, true, info);
	p = realloc(p, new_size);
	if (leaf) {
		tree->root = bpstartree_leaf_link_to_node(p, info);
	} else {
		tree->root = (struct _bpstartree_node *)p;
	}
	tree->root->root = true;
	tree->root->num_items = num_items;
	return tree->root;
}

void _bpstartree_init(struct _bpstartree *tree)
{
	memset(tree, 0, sizeof(*tree));
}

void _bpstartree_destroy(struct _bpstartree *tree, const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		return;
	}

	free(tree->list);
	tree->list = NULL;

	struct _bpstartree_pos path[BPSTARTREE_MAX_HEIGHT];
	struct _bpstartree_node *node = tree->root;
	for (unsigned int level = tree->height; level-- > 0;) {
		path[level].idx = 0;
		path[level].node = node;
		node = level != 0 ? bpstartree_node_get_child(node, 0, info) : NULL;
	}

	unsigned int level = 0;
	for (;;) {
		// start at leaf
		struct _bpstartree_pos *pos = &path[level];
		// free the leaf and go up, if we are at the end of the current node free it and keep going up
		do {
			bool leaf = level == 0;
			if (leaf && info->destroy_key) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_key(bpstartree_node_key(pos->node, i, info));
				}
			}
			if (leaf && info->destroy_value) {
				for (unsigned int i = 0; i < pos->node->num_items; i++) {
					info->destroy_value(bpstartree_node_value(pos->node, i, info));
				}
			}
			bpstartree_free_node(pos->node, leaf, info);
			if (++level == tree->height) {
				tree->root = NULL;
				tree->height = 0;
				return;
			}
			pos = &path[level];
		} while (pos->idx >= pos->node->num_items);
		// descend into the leftmost child of the right child
		pos->idx++;
		do {
			struct _bpstartree_node *child = bpstartree_node_get_child(pos->node, pos->idx, info);
			pos = &path[--level];
			pos->node = child;
			pos->idx = 0;
		} while (level != 0);
	}
}

_bpstartree_kv_t _bpstartree_find(const struct _bpstartree *tree, const void *key,
				  const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		return (_bpstartree_kv_t) { NULL, NULL };
	}
	struct _bpstartree_node *node = tree->root;
	unsigned int level = tree->height;
	void *k = NULL;
	void *v = NULL;
	for (;;) {
		unsigned int idx;
		bool found = bpstartree_node_search(node, key, &idx, info);
		if (--level == 0) {
			if (found) {
				k = bpstartree_node_key(node, idx, info);
				v = bpstartree_node_value(node, idx, info);
			}
			break;
		}
		idx += found;
		node = bpstartree_node_get_child(node, idx, info);
	}
	return (_bpstartree_kv_t) { k, v };
}

_bpstartree_kv_t _bpstartree_get_leftmost_rightmost(const struct _bpstartree *tree, bool leftmost,
						    const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		return (_bpstartree_kv_t) { NULL, NULL };
	}
	struct _bpstartree_leaf_link *link = leftmost ? tree->list->next : tree->list->prev;
	struct _bpstartree_node *node = bpstartree_leaf_link_to_node(link, info);
	unsigned int idx = leftmost ? 0 : node->num_items - 1;
	return (_bpstartree_kv_t) {
		bpstartree_node_key(node, idx, info),
		bpstartree_node_value(node, idx, info)
	};
}

static void bpstartree_node_shift_keys_right(struct _bpstartree_node *node, unsigned int idx,
					     const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_key(node, idx + 1, info),
		bpstartree_node_key(node, idx, info),
		(node->num_items - idx) * info->key_size);
}

static void bpstartree_node_shift_children_right(struct _bpstartree_node *node, unsigned int idx,
						 const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_children(node, info) + idx + 1,
		bpstartree_node_children(node, info) + idx,
		(node->num_items + 1 - idx) * sizeof(struct _bpstartree_node *));
}

static void bpstartree_node_shift_values_right(struct _bpstartree_node *node, unsigned int idx,
					       const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_value(node, idx + 1, info),
		bpstartree_node_value(node, idx, info),
		(node->num_items - idx) * info->value_size);
}

static void bpstartree_node_shift_keys_left(struct _bpstartree_node *node, unsigned int idx,
					    const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_key(node, idx, info),
		bpstartree_node_key(node, idx + 1, info),
		(node->num_items - idx - 1) * info->key_size);
}

static void bpstartree_node_shift_children_left(struct _bpstartree_node *node, unsigned int idx,
						const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_children(node, info) + idx,
		bpstartree_node_children(node, info) + idx + 1,
		(node->num_items - idx) * sizeof(struct _bpstartree_node *));
}

static void bpstartree_node_shift_values_left(struct _bpstartree_node *node, unsigned int idx,
					      const struct bplusstartree_info *info)
{
	memmove(bpstartree_node_value(node, idx, info),
		bpstartree_node_value(node, idx + 1, info),
		(node->num_items - idx - 1) * info->value_size);
}

static void bpstartree_node_insert_unchecked(struct _bpstartree_node *node, unsigned int idx,
					     void *key, struct _bpstartree_node *right,
					     const struct bplusstartree_info *info)
{
	// assert(node->num_items < (node->root ? bpstartree_root_max_items(info) : info->max_items));
	// assert(idx <= node->num_items);
	// assert(right);
	bpstartree_node_shift_keys_right(node, idx, info);
	bpstartree_node_set_key(node, idx, key, info);
	bpstartree_node_shift_children_right(node, idx + 1, info);
	bpstartree_node_set_child(node, idx + 1, right, info);
	node->num_items++;
}

static void bpstartree_leaf_insert_unchecked(struct _bpstartree_node *node, unsigned int idx, void *key,
					     void *value, const struct bplusstartree_info *info)
{
	// assert(node->num_items < (node->root ? bpstartree_root_max_items(info) : info->max_items));
	// assert(idx <= node->num_items);
	bpstartree_node_shift_keys_right(node, idx, info);
	bpstartree_node_set_key(node, idx, key, info);
	bpstartree_node_shift_values_right(node, idx, info);
	bpstartree_node_set_value(node, idx, value, info);
	node->num_items++;
}

static void bpstartree_move_item_left(struct _bpstartree_node *left, struct _bpstartree_node *right,
				      struct _bpstartree_node *parent, unsigned int idx_in_parent,
				      bool leaf, const struct bplusstartree_info *info)
{
	// assert(left == bpstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bpstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(left->num_items < info->max_items);
	// assert(right->num_items > info->min_items);

	if (leaf) {
		bpstartree_node_copy_key(left, left->num_items, right, 0, info);
		bpstartree_node_shift_keys_left(right, 0, info);
		bpstartree_node_copy_value(left, left->num_items, right, 0, info);
		bpstartree_node_shift_values_left(right, 0, info);
		bpstartree_node_copy_key(parent, idx_in_parent, right, 0, info);
	} else {
		bpstartree_node_copy_key(left, left->num_items, parent, idx_in_parent, info);
		bpstartree_node_copy_key(parent, idx_in_parent, right, 0, info);
		bpstartree_node_shift_keys_left(right, 0, info);
		bpstartree_node_copy_child(left, left->num_items + 1, right, 0, info);
		bpstartree_node_shift_children_left(right, 0, info);
	}
	right->num_items--;
	left->num_items++;
}

static void bpstartree_move_item_right(struct _bpstartree_node *left, struct _bpstartree_node *right,
				       struct _bpstartree_node *parent, unsigned int idx_in_parent,
				       bool leaf, const struct bplusstartree_info *info)
{
	// assert(left == bpstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bpstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(right->num_items < info->max_items);
	// assert(left->num_items > info->min_items);

	if (leaf) {
		bpstartree_node_shift_keys_right(right, 0, info);
		bpstartree_node_copy_key(right, 0, left, left->num_items - 1, info);
		bpstartree_node_shift_values_right(right, 0, info);
		bpstartree_node_copy_value(right, 0, left, left->num_items - 1, info);
		bpstartree_node_copy_key(parent, idx_in_parent, right, 0, info);
	} else {
		bpstartree_node_shift_keys_right(right, 0, info);
		bpstartree_node_copy_key(right, 0, parent, idx_in_parent, info);
		bpstartree_node_copy_key(parent, idx_in_parent, left, left->num_items - 1, info);
		bpstartree_node_shift_children_right(right, 0, info);
		bpstartree_node_copy_child(right, 0, left, left->num_items, info);
	}
	right->num_items++;
	left->num_items--;
}

static void bpstartree_merge(struct _bpstartree_node *left, struct _bpstartree_node *mid,
			     struct _bpstartree_node *right, struct _bpstartree_node *parent,
			     unsigned int idx_in_parent, bool leaf, const struct bplusstartree_info *info)
{
	// assert(left == bpstartree_node_get_child(parent, idx_in_parent - 1, info));
	// assert(mid == bpstartree_node_get_child(parent, idx_in_parent, info));
	// assert(right == bpstartree_node_get_child(parent, idx_in_parent + 1, info));
	// assert(left->num_items <= info->min_items);
	// assert(mid->num_items <= info->min_items);
	// assert(right->num_items <= info->min_items);
	// assert(left->num_items + mid->num_items + right->num_items == 3 * info->min_items - 1);

	unsigned int new_left_num_items = 3 * info->min_items / 2;
	unsigned int new_mid_num_items = 3 * info->min_items - new_left_num_items;

	if (leaf) {
		new_mid_num_items--;
		unsigned int n = new_left_num_items - left->num_items;
		// assert(mid->num_items >= n);
		bpstartree_node_copy_keys(left, left->num_items, mid, 0, n, info);
		memmove(bpstartree_node_key(mid, 0, info),
			bpstartree_node_key(mid, n, info),
			(mid->num_items - n) * info->key_size);
		n = mid->num_items - n;
		// assert(n + right->num_items <= info->max_items);
		// assert(n + right->num_items == new_mid_num_items);
		bpstartree_node_copy_keys(mid, n, right, 0, right->num_items, info);

		bpstartree_node_copy_key(parent, idx_in_parent - 1, mid, 0, info);
		bpstartree_node_shift_keys_left(parent, idx_in_parent, info);
		bpstartree_node_shift_children_left(parent, idx_in_parent + 1, info);

		n = new_left_num_items - left->num_items;
		bpstartree_node_copy_values(left, left->num_items, mid, 0, n, info);
		memmove(bpstartree_node_value(mid, 0, info),
			bpstartree_node_value(mid, n, info),
			(mid->num_items - n) * info->value_size);
		n = mid->num_items - n;
		bpstartree_node_copy_values(mid, n, right, 0, right->num_items, info);

		struct _bpstartree_leaf_link *mid_link = bpstartree_leaf_link(mid, info);
		struct _bpstartree_leaf_link *right_link = bpstartree_leaf_link(right, info);
		mid_link->next = right_link->next;
		right_link->next->prev = mid_link;
	} else {
		bpstartree_node_copy_key(left, left->num_items, parent, idx_in_parent - 1, info);
		unsigned int n = new_left_num_items - (left->num_items + 1);
		// assert(mid->num_items > n);
		bpstartree_node_copy_keys(left, left->num_items + 1, mid, 0, n, info);
		bpstartree_node_copy_key(parent, idx_in_parent - 1, mid, n, info);
		n++;
		memmove(bpstartree_node_key(mid, 0, info),
			bpstartree_node_key(mid, n, info),
			(mid->num_items - n) * info->key_size);
		// assert(mid->num_items >= n);
		n = mid->num_items - n;
		bpstartree_node_copy_key(mid, n, parent, idx_in_parent, info);
		n++;
		// assert(n + right->num_items == new_mid_num_items);
		bpstartree_node_copy_keys(mid, n, right, 0, right->num_items, info);

		bpstartree_node_shift_keys_left(parent, idx_in_parent, info);
		bpstartree_node_shift_children_left(parent, idx_in_parent + 1, info);

		n = new_left_num_items - left->num_items;
		bpstartree_node_copy_children(left, left->num_items + 1, mid, 0, n, info);
		memmove(bpstartree_node_children(mid, info),
			bpstartree_node_children(mid, info) + n,
			(mid->num_items + 1 - n) * sizeof(struct _bpstartree_node *));
		n = mid->num_items + 1 - n;
		bpstartree_node_copy_children(mid, n, right, 0, right->num_items + 1, info);
		// assert(n + right->num_items + 1 == new_mid_num_items + 1);
	}

	parent->num_items--;
	left->num_items = new_left_num_items;
	mid->num_items = new_mid_num_items;
	bpstartree_free_node(right, leaf, info);
}

static void bpstartree_merge_into_root(struct _bpstartree *tree, bool leaf,
				       const struct bplusstartree_info *info)
{
	struct _bpstartree_node *root = tree->root;
	struct _bpstartree_node *left = bpstartree_node_get_child(root, 0, info);
	struct _bpstartree_node *right = bpstartree_node_get_child(root, 1, info);
	// assert(root->num_items == 1);
	// assert(left->num_items <= info->min_items);
	// assert(right->num_items <= info->min_items);
	// assert(left->num_items + right->num_items == 2 * info->min_items - 1);
	// assert(left->leaf == right->leaf);

	if (leaf) {
		// assert(tree->height == 2);
		root = bpstartree_realloc_root(tree, true, info);
		bpstartree_node_copy_keys(root, 0, left, 0, left->num_items, info);
		bpstartree_node_copy_keys(root, left->num_items, right, 0, right->num_items, info);

		bpstartree_node_copy_values(root, 0, left, 0, left->num_items, info);
		bpstartree_node_copy_values(root, left->num_items, right, 0, right->num_items, info);

		struct _bpstartree_leaf_link *root_link = bpstartree_leaf_link(root, info);
		root_link->prev = tree->list;
		root_link->next = tree->list;
		tree->list->prev = root_link;
		tree->list->next = root_link;
	} else {
		bpstartree_node_copy_key(root, left->num_items, root, 0, info);
		bpstartree_node_copy_keys(root, 0, left, 0, left->num_items, info);
		bpstartree_node_copy_keys(root, left->num_items + 1, right, 0, right->num_items, info);

		bpstartree_node_copy_children(root, 0, left, 0, left->num_items + 1, info);
		bpstartree_node_copy_children(root, left->num_items + 1, right, 0, right->num_items + 1, info);
	}

	root->num_items = 2 * info->min_items - leaf;
	// root->leaf = leaf;
	tree->height--;
	bpstartree_free_node(left, leaf, info);
	bpstartree_free_node(right, leaf, info);
}

bool _bpstartree_delete(struct _bpstartree *tree, enum _bpstartree_deletion_mode mode, const void *key,
			void *ret_key, void *ret_value, const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		return false;
	}
	struct _bpstartree_node *node = tree->root;
	unsigned int level = tree->height - 1;
	struct _bpstartree_pos path[BPSTARTREE_MAX_HEIGHT];
	unsigned int idx;
	bool leaf = false;
	int internal_node_key_found_level = -1;
	for (;;) {
		leaf = level == 0;
		bool found = false;
		switch (mode) {
		case __BPSTARTREE_DELETE_KEY:
			found = bpstartree_node_search(node, key, &idx, info);
			if (leaf && !found) {
				return false;
			}
			if (!leaf && found) {
				// assert(internal_node_key_found_level < 0);
				internal_node_key_found_level = level;
			}
			break;
		case __BPSTARTREE_DELETE_MIN:
			idx = 0;
			break;
		case __BPSTARTREE_DELETE_MAX:
			idx = leaf ? node->num_items - 1 : node->num_items;
			break;
		}
		if (leaf) {
			break;
		}
		idx += found;
		path[level].idx = idx;
		path[level].node = node;
		level--;
		node = bpstartree_node_get_child(node, idx, info);
	}

	if (internal_node_key_found_level >= 0) {
		struct _bpstartree_node *internal_node = path[internal_node_key_found_level].node;
		unsigned int internal_idx = path[internal_node_key_found_level].idx - 1;
		// assert(info->cmp(bpstartree_node_key(internal_node, internal_idx, info), key) == 0);

		// if the compiler constant folds 'info', it can remove the branch below if info->min_items > 1
		// assert(node->num_items > 1);
		bpstartree_node_copy_key(internal_node, internal_idx, node, 1, info);
	}

	if (ret_key) {
		bpstartree_node_get_key(ret_key, node, idx, info);
	} else if (info->destroy_key) {
		info->destroy_key(bpstartree_node_key(node, idx, info));
	}
	if (ret_value) {
		bpstartree_node_get_value(ret_value, node, idx, info);
	} else if (info->destroy_value) {
		info->destroy_value(bpstartree_node_value(node, idx, info));
	}
	bpstartree_node_shift_keys_left(node, idx, info);
	bpstartree_node_shift_values_left(node, idx, info);
	// assert(node->num_items != 0);
	node->num_items--;

	for (leaf = true; ++level != tree->height; leaf = false) {
		if (node->num_items >= info->min_items) {
			return true;
		}

		node = path[level].node;
		idx = path[level].idx;

		struct _bpstartree_node *left = NULL;
		struct _bpstartree_node *mid = bpstartree_node_get_child(node, idx, info);
		struct _bpstartree_node *right = NULL;
		if (idx != 0) {
			left = bpstartree_node_get_child(node, idx - 1, info);
		}
		if (idx != node->num_items) {
			right = bpstartree_node_get_child(node, idx + 1, info);
		}

		// assert(left || right);

		if (left && left->num_items > info->min_items) {
			bpstartree_move_item_right(left, mid, node, idx - 1, leaf, info);
			continue;
		}

		if (right && right->num_items > info->min_items) {
			bpstartree_move_item_left(mid, right, node, idx, leaf, info);
			continue;
		}

		if (level == tree->height - 1u && node->num_items == 1) {
			// assert(node == tree->root);
			bpstartree_merge_into_root(tree, leaf, info);
			// node is not valid anymore
			return true;
		}

		// assert(info->min_items >= 2);

		if (!left) {
			// assert(idx == 0);
			// assert(node->num_items >= 2);
			struct _bpstartree_node *rightright = bpstartree_node_get_child(node, idx + 2, info);
			if (rightright->num_items > info->min_items) {
				bpstartree_move_item_left(right, rightright, node, idx + 1, leaf, info);
				bpstartree_move_item_left(mid, right, node, idx, leaf, info);
				continue;
			}
			left = mid;
			mid = right;
			right = rightright;
			idx++;
		} else if (!right) {
			// assert(idx == node->num_items);
			// assert(idx >= 2);
			struct _bpstartree_node *leftleft = bpstartree_node_get_child(node, idx - 2, info);
			if (leftleft->num_items > info->min_items) {
				bpstartree_move_item_right(leftleft, left, node, idx - 2, leaf, info);
				bpstartree_move_item_right(left, mid, node, idx - 1, leaf, info);
				continue;
			}
			right = mid;
			mid = left;
			left = leftleft;
			idx--;
		}

		bpstartree_merge(left, mid, right, node, idx, leaf, info);
	}

	// assert(node == tree->root);

	if (node->num_items == 0) {
		tree->root = NULL;
		free(tree->list);
		tree->list = NULL;
		// assert(tree->height == 1);
		tree->height = 0;
		bpstartree_free_node(node, true, info);
	}

	return true;
}

static struct _bpstartree_node *bpstartree_leaf_insert(struct _bpstartree *tree,
						       struct _bpstartree_pos path[static BPSTARTREE_MAX_HEIGHT],
						       void *key, void *value,
						       const struct bplusstartree_info *info)
{
	struct _bpstartree_node *node = path[0].node;
	unsigned int idx = path[0].idx;
	const unsigned int root_max_items = bpstartree_root_max_items(info);
	if (node->num_items != (node->root ? root_max_items : info->max_items)) {
		bpstartree_leaf_insert_unchecked(node, idx, key, value, info);
		return NULL;
	}

	if (node->root) {
		// assert(node == tree->root);
		// assert(tree->height == 1);
		struct _bpstartree_node *left_child = bpstartree_new_node(true, false, info);
		struct _bpstartree_node *right_child = bpstartree_new_node(true, false, info);
		left_child->num_items = root_max_items / 2;
		right_child->num_items = root_max_items / 2;
		node->num_items = 1;
		tree->height = 2;
		bpstartree_node_copy_keys(left_child, 0, node, 0, left_child->num_items, info);
		bpstartree_node_copy_keys(right_child, 0, node, left_child->num_items,
					  right_child->num_items, info);
		bpstartree_node_copy_values(left_child, 0, node, 0, left_child->num_items, info);
		bpstartree_node_copy_values(right_child, 0, node, left_child->num_items,
					    right_child->num_items, info);
		if (idx < left_child->num_items) {
			bpstartree_leaf_insert_unchecked(left_child, idx, key, value, info);
		} else {
			idx -= left_child->num_items;
			// assert(idx <= right_child->num_items);
			bpstartree_leaf_insert_unchecked(right_child, idx, key, value, info);
		}

		struct _bpstartree_leaf_link *left_link = bpstartree_leaf_link(left_child, info);
		struct _bpstartree_leaf_link *right_link = bpstartree_leaf_link(right_child, info);
		tree->list->next = left_link;
		left_link->next = right_link;
		right_link->next = tree->list;
		tree->list->prev = right_link;
		right_link->prev = left_link;
		left_link->prev = tree->list;

		node = bpstartree_realloc_root(tree, false, info);
		bpstartree_node_set_child(node, 0, left_child, info);
		bpstartree_node_set_child(node, 1, right_child, info);
		bpstartree_node_copy_key(node, 0, right_child, 0, info);
		return NULL;
	}

	// assert(tree->height > 1);
	struct _bpstartree_node *parent = path[1].node;
	unsigned int idx_in_parent = path[1].idx;

	struct _bpstartree_node *left = NULL;
	if (idx_in_parent != 0) {
		left = bpstartree_node_get_child(parent, idx_in_parent - 1, info);
		if (left->num_items != info->max_items) {
			// assert(idx != 0);
			// inlined bpstartree_move_item_left here, because the copy to the parent needs to
			// happen after the insertion (in case idx == 1)
			bpstartree_node_copy_key(left, left->num_items, node, 0, info);
			bpstartree_node_shift_keys_left(node, 0, info);
			bpstartree_node_copy_value(left, left->num_items, node, 0, info);
			bpstartree_node_shift_values_left(node, 0, info);
			node->num_items--;
			left->num_items++;
			bpstartree_leaf_insert_unchecked(node, idx - 1, key, value, info);
			bpstartree_node_copy_key(parent, idx_in_parent - 1, node, 0, info);
			return NULL;
		}
	}
	struct _bpstartree_node *right = NULL;
	if (idx_in_parent != parent->num_items) {
		right = bpstartree_node_get_child(parent, idx_in_parent + 1, info);
		if (right->num_items != info->max_items) {
			if (idx == node->num_items) {
				bpstartree_leaf_insert_unchecked(right, 0, key, value, info);
				bpstartree_node_set_key(parent, idx_in_parent, key, info);
				return NULL;
			}
			bpstartree_move_item_right(node, right, parent, idx_in_parent, true, info);
			bpstartree_leaf_insert_unchecked(node, idx, key, value, info);
			// assert(idx != 0 || idx_in_parent == 0);
			return NULL;
		}
	}

	if (!left) {
		left = node;
	} else {
		right = node;
		idx += info->max_items;
		idx_in_parent--;
	}

	struct _bpstartree_node *insertion_node;
	struct _bpstartree_node *new_node = bpstartree_new_node(true, false, info);
	left->num_items = 2 * info->max_items - 2 * info->min_items;
	right->num_items = info->min_items;
	new_node->num_items = info->min_items;
	// this is necessary for the case where info->max_items == 4 and insertion_node == left, because
	// otherwise left->num_items == info->max_items
	// TODO maybe check left->num_items == info->max_items instead?
	if (left->num_items > right->num_items) {
		left->num_items--;
		right->num_items++;
	}
	if (idx < left->num_items) {
		insertion_node = left;
	} else {
		idx -= left->num_items;
		if (idx < right->num_items) {
			insertion_node = right;
		} else {
			idx -= right->num_items;
			// assert(idx <= new_node->num_items);
			insertion_node = new_node;
		}
	}

	unsigned int n = info->max_items - new_node->num_items;
	bpstartree_node_copy_keys(new_node, 0, right, n, new_node->num_items, info);
	bpstartree_node_copy_values(new_node, 0, right, n, new_node->num_items, info);
	memmove(bpstartree_node_key(right, right->num_items - n, info),
		bpstartree_node_key(right, 0, info),
		n * info->key_size);
	memmove(bpstartree_node_value(right, right->num_items - n, info),
		bpstartree_node_value(right, 0, info),
		n * info->value_size);
	// assert(n <= right->num_items);
	n = right->num_items - n;
	bpstartree_node_copy_keys(right, 0, left, info->max_items - n, n, info);
	bpstartree_node_copy_values(right, 0, left, info->max_items - n, n, info);
	// assert(n < info->max_items);
	n = info->max_items - n;
	// assert(n == left->num_items);
	bpstartree_leaf_insert_unchecked(insertion_node, idx, key, value, info);
	bpstartree_node_copy_key(parent, idx_in_parent, right, 0, info);

	struct _bpstartree_leaf_link *right_link = bpstartree_leaf_link(right, info);
	struct _bpstartree_leaf_link *new_link = bpstartree_leaf_link(new_node, info);
	struct _bpstartree_leaf_link *next_link = right_link->next;
	new_link->prev = right_link;
	new_link->next = next_link;
	next_link->prev = new_link;
	right_link->next = new_link;

	path[1].idx = idx_in_parent + 1;
	return new_node;
}

static bool bpstartree_node_spill_left(struct _bpstartree_node *left_sibling,
				       struct _bpstartree_node *right_sibling, unsigned int idx,
				       void *key, struct _bpstartree_node *right,
				       struct _bpstartree_node *parent, unsigned int idx_in_parent,
				       const struct bplusstartree_info *info)
{
	if (left_sibling->num_items == info->max_items) {
		return false;
	}
	if (idx == 0) {
		bpstartree_node_insert_unchecked(left_sibling, left_sibling->num_items,
						 bpstartree_node_key(parent, idx_in_parent - 1, info),
						 bpstartree_node_get_child(right_sibling, 0, info),
						 info);
		bpstartree_node_set_key(parent, idx_in_parent - 1, key, info);
		bpstartree_node_set_child(right_sibling, 0, right, info);
		return true;
	}
	bpstartree_move_item_left(left_sibling, right_sibling, parent, idx_in_parent - 1, false, info);
	bpstartree_node_insert_unchecked(right_sibling, idx - 1, key, right, info);
	return true;
}

static bool bpstartree_node_spill_right(struct _bpstartree_node *left_sibling,
					struct _bpstartree_node *right_sibling, unsigned int idx,
					void *key, struct _bpstartree_node *right,
					struct _bpstartree_node *parent, unsigned int idx_in_parent,
					const struct bplusstartree_info *info)
{
	if (right_sibling->num_items == info->max_items) {
		return false;
	}
	if (idx == info->max_items) {
		bpstartree_node_shift_keys_right(right_sibling, 0, info);
		bpstartree_node_set_key(right_sibling, 0,
					bpstartree_node_key(parent, idx_in_parent, info),
					info);
		bpstartree_node_shift_children_right(right_sibling, 0, info);
		bpstartree_node_set_child(right_sibling, 0, right, info);
		right_sibling->num_items++;
		bpstartree_node_set_key(parent, idx_in_parent, key, info);
		return true;
	}
	bpstartree_move_item_right(left_sibling, right_sibling, parent, idx_in_parent, false, info);
	bpstartree_node_insert_unchecked(left_sibling, idx, key, right, info);
	return true;
}

static struct _bpstartree_node *bpstartree_node_split_and_insert(struct _bpstartree_node *left_sibling,
								 struct _bpstartree_node *right_sibling,
								 unsigned int idx, void *key,
								 struct _bpstartree_node *right,
								 struct _bpstartree_node *parent,
								 unsigned int idx_in_parent,
								 const struct bplusstartree_info *info)
{
	// assert(left_sibling == bpstartree_node_get_child(parent, idx_in_parent, info));
	// assert(left_sibling->num_items == info->max_items);
	// assert(right_sibling->num_items == info->max_items);
	struct _bpstartree_node *new_node = bpstartree_new_node(!right, false, info);
	left_sibling->num_items = 2 * info->max_items - 2 * info->min_items; // + 2 - 2
	right_sibling->num_items = info->min_items;
	new_node->num_items = info->min_items;
	// assert(left_sibling->num_items >= info->min_items);
	// assert(left_sibling->num_items <= info->max_items);

	struct _bpstartree_node *key_insertion_node;
	struct _bpstartree_node *right_insertion_node;
	unsigned int right_insertion_idx;
	// unsigned int left_num_children = left_sibling->num_items + 1;
	unsigned int right_num_children = right_sibling->num_items + 1;
	unsigned int new_num_children = new_node->num_items + 1;
	if (idx < left_sibling->num_items) {
		left_sibling->num_items--;
		key_insertion_node = left_sibling;
		right_insertion_node = left_sibling;
		right_insertion_idx = idx + 1;
		// left_num_children--;
	} else if (idx == left_sibling->num_items) {
		idx = idx_in_parent;
		key_insertion_node = parent;
		right_insertion_node = right_sibling;
		right_insertion_idx = 0;
		right_num_children--;
	} else {
		idx -= left_sibling->num_items + 1;
		if (idx < right_sibling->num_items) {
			right_sibling->num_items--;
			key_insertion_node = right_sibling;
			right_insertion_node = right_sibling;
			right_insertion_idx = idx + 1;
			right_num_children--;
		} else if (idx == right_sibling->num_items) {
			idx = -1;
			key_insertion_node = NULL;
			right_insertion_node = new_node;
			right_insertion_idx = 0;
			new_num_children--;
		} else {
			idx -= right_sibling->num_items + 1;
			// assert(idx < new_node->num_items);
			new_node->num_items--;
			key_insertion_node = new_node;
			right_insertion_node = new_node;
			right_insertion_idx = idx + 1;
			new_num_children--;
		}
	}

	void *temp = alloca(info->key_size);
	unsigned int n = info->max_items - new_node->num_items;
	bpstartree_node_copy_keys(new_node, 0, right_sibling, n, new_node->num_items, info);
	// assert(n > 0);
	if (key_insertion_node) {
		n--;
		bpstartree_node_get_key(temp, right_sibling, n, info);
	}
	memmove(bpstartree_node_key(right_sibling, right_sibling->num_items - n, info),
		bpstartree_node_key(right_sibling, 0, info),
		n * info->key_size);
	n = right_sibling->num_items - n;
	if (n > 0) {
		n--;
		bpstartree_node_copy_key(right_sibling, n, parent, idx_in_parent, info);
		bpstartree_node_copy_keys(right_sibling, 0, left_sibling, info->max_items - n, n, info);
	}
	n = info->max_items - n;
	if (n != left_sibling->num_items && key_insertion_node != parent) {
		n--;
		bpstartree_node_copy_key(parent, idx_in_parent, left_sibling, n, info);
	}
	// assert(n == left_sibling->num_items);
	if (key_insertion_node) {
		if (key_insertion_node != parent) {
			bpstartree_node_shift_keys_right(key_insertion_node, idx, info);
		}
		bpstartree_node_set_key(key_insertion_node, idx, key, info);
		memcpy(key, temp, info->key_size);
	}

	n = info->max_items + 1 - new_num_children;
	bpstartree_node_copy_children(new_node, 0, right_sibling, n, new_num_children, info);
	memmove(bpstartree_node_children(right_sibling, info) + right_num_children - n,
		bpstartree_node_children(right_sibling, info),
		n * sizeof(struct _bpstartree_node *));
	n = right_num_children - n;
	bpstartree_node_copy_children(right_sibling, 0, left_sibling, info->max_items + 1 - n,
				      n, info);
	n = info->max_items + 1 - n;
	// assert(n == left_num_children);
	bpstartree_node_shift_children_right(right_insertion_node, right_insertion_idx, info);
	bpstartree_node_set_child(right_insertion_node, right_insertion_idx, right, info);
	if (key_insertion_node && key_insertion_node != parent) {
		key_insertion_node->num_items++;
	}

	return new_node;
}

static void bpstartree_node_split_and_insert_root(struct _bpstartree *tree, unsigned int idx, void *key,
						  struct _bpstartree_node *right, bool bulk_loading,
						  const struct bplusstartree_info *info)
{
	const unsigned int root_max_items = bpstartree_root_max_items(info);
	struct _bpstartree_node *root = tree->root;
	// assert(root->num_items == root_max_items);
	struct _bpstartree_node *left_child = bpstartree_new_node(false, false, info);
	struct _bpstartree_node *right_child = bpstartree_new_node(false, false, info);

	if (bulk_loading) {
		left_child->num_items = info->max_items;
	} else {
		left_child->num_items = root_max_items / 2;
	}
	right_child->num_items = root_max_items - left_child->num_items;
	root->num_items = 1;

	if (idx < left_child->num_items) {
		left_child->num_items--;
		bpstartree_node_copy_keys(left_child, 0, root, 0, left_child->num_items, info);
		bpstartree_node_copy_key(root, 0, root, left_child->num_items, info);
		bpstartree_node_copy_keys(right_child, 0, root, left_child->num_items + 1,
					  right_child->num_items, info);

		bpstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
		bpstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
					      right_child->num_items + 1, info);

		bpstartree_node_insert_unchecked(left_child, idx, key, right, info);
	} else if (idx > left_child->num_items) {
		idx -= left_child->num_items + 1;
		right_child->num_items--;
		bpstartree_node_copy_keys(left_child, 0, root, 0, left_child->num_items, info);
		bpstartree_node_copy_key(root, 0, root, left_child->num_items, info);
		bpstartree_node_copy_keys(right_child, 0, root, left_child->num_items + 1,
					  right_child->num_items, info);

		bpstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
		bpstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
					      right_child->num_items + 1, info);

		bpstartree_node_insert_unchecked(right_child, idx, key, right, info);
	} else {
		bpstartree_node_copy_keys(left_child, 0, root, 0, left_child->num_items, info);
		bpstartree_node_copy_keys(right_child, 0, root, left_child->num_items,
					  right_child->num_items, info);

		bpstartree_node_copy_children(left_child, 0, root, 0, left_child->num_items + 1, info);
		bpstartree_node_copy_children(right_child, 0, root, left_child->num_items + 1,
					      right_child->num_items, info);

		// right_child->num_items is 0 only when bulk_loading and max_keys == 4
		if (right_child->num_items != 0) {
			right_child->num_items--;
			bpstartree_node_shift_children_right(right_child, 0, info);
			right_child->num_items++;
		}
		bpstartree_node_set_child(right_child, 0, right, info);

		bpstartree_node_set_key(root, 0, key, info);
	}

	// assert((unsigned int)left_child->num_items + right_child->num_items + root->num_items == root_max_items + 1);

	bpstartree_node_set_child(root, 0, left_child, info);
	bpstartree_node_set_child(root, 1, right_child, info);
	// root->leaf = false;
	tree->height++;
}

static void bpstartree_internal_insert_and_rebalance(struct _bpstartree *tree, void *key,
						     struct _bpstartree_node *right,
						     struct _bpstartree_pos path[static BPSTARTREE_MAX_HEIGHT],
						     const struct bplusstartree_info *info)
{
	// assert(tree->height > 1);
	unsigned int idx = path[1].idx;
	for (unsigned int level = 1; level < tree->height - 1u; level++) {
		struct _bpstartree_node *node = path[level].node;

		// assert(idx <= node->num_items);
		if (node->num_items < info->max_items) {
			bpstartree_node_insert_unchecked(node, idx, key, right, info);
			return;
		}

		struct _bpstartree_node *parent = path[level + 1].node;
		unsigned int idx_in_parent = path[level + 1].idx;
		struct _bpstartree_node *left_sibling = NULL;
		struct _bpstartree_node *right_sibling = NULL;
		if (idx_in_parent != 0) {
			left_sibling = bpstartree_node_get_child(parent, idx_in_parent - 1, info);
			if (bpstartree_node_spill_left(left_sibling, node, idx, key, right,
						       parent, idx_in_parent, info)) {
				return;
			}
		}
		if (idx_in_parent != parent->num_items) {
			right_sibling = bpstartree_node_get_child(parent, idx_in_parent + 1, info);
			if (bpstartree_node_spill_right(node, right_sibling, idx, key, right,
							parent, idx_in_parent, info)) {
				return;
			}
		}
		if (left_sibling) {
			right_sibling = node;
			idx += info->max_items + 1;
			idx_in_parent--;
		} else {
			left_sibling = node;
		}
		right = bpstartree_node_split_and_insert(left_sibling, right_sibling, idx, key, right,
							 parent, idx_in_parent, info);
		idx = idx_in_parent + 1;
	}
	if (tree->root->num_items < bpstartree_root_max_items(info)) {
		bpstartree_node_insert_unchecked(tree->root, idx, key, right, info);
		return;
	}
	bpstartree_node_split_and_insert_root(tree, idx, key, right, false, info);
}

static void bpstartree_insert_and_rebalance(struct _bpstartree *tree, void *key, void *value,
					    struct _bpstartree_pos path[static BPSTARTREE_MAX_HEIGHT],
					    const struct bplusstartree_info *info)
{
	struct _bpstartree_node *right = bpstartree_leaf_insert(tree, path, key, value, info);
	if (!right) {
		return;
	}
	bpstartree_node_get_key(key, right, 0, info);
	bpstartree_internal_insert_and_rebalance(tree, key, right, path, info);
}

bool _bpstartree_insert(struct _bpstartree *tree, void *key, void *value, bool update,
			const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		tree->root = bpstartree_new_node(true, true, info);
		// assert(!tree->list);
		free(tree->list);
		tree->list = malloc(sizeof(*tree->list));
		struct _bpstartree_leaf_link *root_link = bpstartree_leaf_link(tree->root, info);
		root_link->prev = tree->list;
		root_link->next = tree->list;
		tree->list->prev = root_link;
		tree->list->next = root_link;
		tree->height = 1;
		bpstartree_node_set_key(tree->root, 0, key, info);
		bpstartree_node_set_value(tree->root, 0, value, info);
		tree->root->num_items = 1;
		return true;
	}
	struct _bpstartree_node *node = tree->root;
	struct _bpstartree_pos path[BPSTARTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	bool found;
	for (;;) {
		found = bpstartree_node_search(node, key, &idx, info);
		if (found && !update) {
			return false;
		}
		path[level].node = node;
		if (level == 0) {
			path[level].idx = idx;
			break;
		}
		// if we find the key we go to the right subtree, which means incrementing idx by 1
		idx += found;
		path[level].idx = idx;
		level--;
		node = bpstartree_node_get_child(node, idx, info);
	}
	if (found) {
		// assert(update);
		if (info->destroy_key) {
			info->destroy_key(bpstartree_node_key(node, idx, info));
		}
		if (info->destroy_value) {
			info->destroy_value(bpstartree_node_value(node, idx, info));
		}
		bpstartree_node_set_key(node, idx, key, info);
		bpstartree_node_set_value(node, idx, value, info);
		return false;
	}
	bpstartree_insert_and_rebalance(tree, key, value, path, info);
	return true;
}

bool _bpstartree_insert_sequential(struct _bpstartree *tree, void *key, void *value,
				   const struct bplusstartree_info *info)
{
	if (tree->height == 0) {
		return _bpstartree_insert(tree, key, value, false, info);
	}
	struct _bpstartree_node *node = tree->root;
	struct _bpstartree_pos path[BPSTARTREE_MAX_HEIGHT];
	unsigned int level = tree->height - 1;
	unsigned int idx;
	for (;;) {
		idx = node->num_items;
		if (unlikely(info->cmp(key, bpstartree_node_key(node, idx - 1, info)) <= 0)) {
			return _bpstartree_insert(tree, key, value, false, info);
		}
		path[level].idx = idx;
		path[level].node = node;
		if (level-- == 0) {
			break;
		}
		node = bpstartree_node_get_child(node, idx, info);
	}
	bpstartree_insert_and_rebalance(tree, key, value, path, info);
	return true;
}

void _bpstartree_bulk_load_start(struct _bpstartree_bulk_load_ctx *ctx)
{
	_bpstartree_init(&ctx->tree);
	ctx->list = NULL;
	ctx->node = NULL;
	ctx->num_items = 0;
}

void _bpstartree_bulk_load_next(struct _bpstartree_bulk_load_ctx *ctx, void *key, void *value,
				const struct bplusstartree_info *info)
{
	if (!ctx->list) {
		ctx->list = malloc(sizeof(*ctx->list));
		ctx->list->next = ctx->list;
		ctx->list->prev = ctx->list;
	}
	if (!ctx->node) {
		ctx->node = bpstartree_new_node(true, false, info);
		struct _bpstartree_leaf_link *link = bpstartree_leaf_link(ctx->node, info);
		link->prev = ctx->list->prev;
		link->next = ctx->list;
		ctx->list->prev->next = link;
		ctx->list->prev = link;
	}
	bpstartree_node_set_key(ctx->node, ctx->node->num_items, key, info);
	bpstartree_node_set_value(ctx->node, ctx->node->num_items, value, info);
	ctx->node->num_items++;
	if (ctx->node->num_items == info->max_items) {
		ctx->node = NULL;
	}
	ctx->num_items++;
}

static void bpstartree_move_items_right(struct _bpstartree_node *left, struct _bpstartree_node *right,
					struct _bpstartree_node *parent, unsigned int idx_in_parent,
					unsigned int num_items, bool leaf,
					const struct bplusstartree_info *info)
{
	// assert(right->num_items + num_items <= info->max_items);
	// assert(left->num_items >= num_items);
	unsigned int n = num_items;
	memmove(bpstartree_node_key(right, n, info),
		bpstartree_node_key(right, 0, info),
		right->num_items * info->key_size);
	if (leaf) {
		bpstartree_node_copy_keys(right, 0, left, left->num_items - n, n, info);

		bpstartree_node_copy_key(parent, idx_in_parent, right, 0, info);

		memmove(bpstartree_node_value(right, n, info),
			bpstartree_node_value(right, 0, info),
			right->num_items * info->value_size);
		bpstartree_node_copy_values(right, 0, left, left->num_items - n, n, info);
	} else {
		n--;
		bpstartree_node_copy_key(right, n, parent, idx_in_parent, info);
		bpstartree_node_copy_keys(right, 0, left, left->num_items - n, n, info);
		n = left->num_items - n - 1;
		bpstartree_node_copy_key(parent, idx_in_parent, left, n, info);

		memmove(bpstartree_node_children(right, info) + num_items,
			bpstartree_node_children(right, info),
			(right->num_items + 1) * sizeof(struct _bpstartree_node *));
		bpstartree_node_copy_children(right, 0, left, left->num_items + 1 - num_items,
					      num_items, info);
	}

	left->num_items -= num_items;
	right->num_items += num_items;
}

static void bpstartree_insert_and_rebalance_bulk(struct _bpstartree *tree, struct _bpstartree_node *right,
						 struct _bpstartree_pos path[static BPSTARTREE_MAX_HEIGHT],
						 const struct bplusstartree_info *info)
{
	void *key = bpstartree_node_key(right, 0, info);
	unsigned int level;
	for (level = 1; level < tree->height - 1u; level++) {
		unsigned int idx = path[level].idx++;
		struct _bpstartree_node *node = path[level].node;

		if (node->num_items < info->max_items) {
			bpstartree_node_insert_unchecked(node, idx, key, right, info);
			return;
		}

		struct _bpstartree_node *new_node = bpstartree_new_node(false, false, info);
		if (right) {
			bpstartree_node_set_child(new_node, 0, right, info);
		}
		right = new_node;
		path[level].node = right;
		path[level].idx = 0;
	}
	unsigned int idx = path[level].idx++;
	// assert(path[level].node == tree->root);
	if (tree->root->num_items < bpstartree_root_max_items(info)) {
		bpstartree_node_insert_unchecked(tree->root, idx, key, right, info);
		return;
	}
	bpstartree_node_split_and_insert_root(tree, idx, key, right, true, info);
	path[level + 1].idx = 1;
	path[level + 1].node = tree->root;
	right = bpstartree_node_get_child(tree->root, 1, info);
	path[level].node = right;
	path[level].idx = right->num_items;
}

void _bpstartree_bulk_load_end(struct _bpstartree_bulk_load_ctx *ctx, const struct bplusstartree_info *info)
{
	if (!ctx->list) {
		return;
	}
	if (ctx->num_items <= bpstartree_root_max_items(info)) {
		// root is leaf in this case
		struct _bpstartree_node *root = bpstartree_new_node(true, true, info);

		struct _bpstartree_leaf_link *first_link = ctx->list->next;
		struct _bpstartree_leaf_link *second_link = first_link->next;
		struct _bpstartree_node *node = bpstartree_leaf_link_to_node(first_link, info);

		bpstartree_node_copy_keys(root, 0, node, 0, node->num_items, info);
		bpstartree_node_copy_values(root, 0, node, 0, node->num_items, info);
		root->num_items = node->num_items;
		bpstartree_free_node(node, true, info);

		if (second_link != ctx->list) {
			// assert(root->num_items == info->max_items);
			node = bpstartree_leaf_link_to_node(second_link, info);

			bpstartree_node_copy_keys(root, info->max_items, node, 0, node->num_items, info);
			bpstartree_node_copy_values(root, info->max_items, node, 0, node->num_items, info);
			root->num_items += node->num_items;
			bpstartree_free_node(node, true, info);
		}

		ctx->tree.root = root;
		ctx->tree.height = 1;
		ctx->tree.list = ctx->list;
		struct _bpstartree_leaf_link *root_link = bpstartree_leaf_link(root, info);
		ctx->tree.list->prev = root_link;
		ctx->tree.list->next = root_link;
		root_link->prev = ctx->tree.list;
		root_link->next = ctx->tree.list;
		return;
	}
	struct _bpstartree_pos path[BPSTARTREE_MAX_HEIGHT];
	ctx->tree.root = bpstartree_new_node(false, true, info);
	ctx->tree.list = ctx->list;
	ctx->tree.height = 2;
	path[1].node = ctx->tree.root;
	path[1].idx = 0;
	struct _bpstartree_leaf_link *link = ctx->list->next;
	path[0].node = bpstartree_leaf_link_to_node(link, info);
	bpstartree_node_set_child(ctx->tree.root, 0, path[0].node, info);
	for (link = link->next;
	     link != ctx->list;
	     link = link->next) {
		struct _bpstartree_node *node = bpstartree_leaf_link_to_node(link, info);
		path[0].node = node;
		bpstartree_insert_and_rebalance_bulk(&ctx->tree, node, path, info);
	}

	struct _bpstartree_node *parent = ctx->tree.root;
	struct _bpstartree_node *node;
	for (unsigned int level = ctx->tree.height - 1; level-- > 0; parent = node) {
		node = path[level].node;
		if (node->num_items >= info->min_items) {
			continue;
		}
		// assert(parent->num_items != 0);
		unsigned int idx_in_parent = parent->num_items - 1;
		struct _bpstartree_node *left = bpstartree_node_get_child(parent, idx_in_parent, info);
		// assert(left->num_items == info->max_items);
		bpstartree_move_items_right(left, node, parent, idx_in_parent,
					    info->min_items - node->num_items,
					    level == 0, info);

		if (left->num_items >= info->min_items) {
			continue;
		}
		// assert(idx_in_parent != 0);
		idx_in_parent--;
		struct _bpstartree_node *leftleft = bpstartree_node_get_child(parent, idx_in_parent, info);
		// assert(leftleft->num_items == info->max_items);
		bpstartree_move_items_right(leftleft, left, parent, idx_in_parent,
					    info->min_items - left->num_items,
					    level == 0, info);
		// assert(leftleft->num_items >= info->min_items);
	}
}

void *_bpstartree_debug_node_key(struct _bpstartree_node *node, unsigned int idx,
				 const struct bplusstartree_info *info)
{
	return bpstartree_node_key(node, idx, info);
}

void *_bpstartree_debug_node_value(struct _bpstartree_node *node, unsigned int idx,
				   const struct bplusstartree_info *info)
{
	return bpstartree_node_value(node, idx, info);
}

struct _bpstartree_node *_bpstartree_debug_node_get_child(struct _bpstartree_node *node, unsigned int idx,
							  const struct bplusstartree_info *info)
{
	return bpstartree_node_get_child(node, idx, info);
}

static struct _bpstartree_node *bpstartree_node_copy(struct _bpstartree_node *node, unsigned int level,
						     struct _bpstartree_leaf_link *list, bool root,
						     const struct bplusstartree_info *info)
{
	struct _bpstartree_node *copy = bpstartree_new_node(level == 0, root, info);
	memcpy(bpstartree_node_key(copy, 0, info),
	       bpstartree_node_key(node, 0, info),
	       node->num_items * info->key_size);
	copy->num_items = node->num_items;
	if (level == 0) {
		memcpy(bpstartree_node_value(copy, 0, info),
		       bpstartree_node_value(node, 0, info),
		       node->num_items * info->value_size);
		struct _bpstartree_leaf_link *link = bpstartree_leaf_link(copy, info);
		list->prev->next = link;
		link->prev = list->prev;
		link->next = list;
		list->prev = link;
		return copy;
	}
	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		struct _bpstartree_node *child = bpstartree_node_get_child(node, i, info);
		struct _bpstartree_node *child_copy = bpstartree_node_copy(child, level - 1, list, false, info);
		bpstartree_node_set_child(copy, i, child_copy, info);
	}
	return copy;
}

struct _bpstartree _bpstartree_debug_copy(const struct _bpstartree *tree, const struct bplusstartree_info *info)
{
	unsigned int height = tree->height;
	struct _bpstartree copy;
	copy.height = height;
	copy.root = NULL;
	if (height != 0) {
		copy.list = malloc(sizeof(*copy.list));
		copy.list->next = copy.list;
		copy.list->prev = copy.list;
		copy.root = bpstartree_node_copy(tree->root, height - 1, copy.list, true, info);
	}
	return copy;
}

size_t _bpstartree_debug_node_size(bool leaf, bool root, const struct bplusstartree_info *info)
{
	return bpstartree_node_size(leaf, root, info);
}
