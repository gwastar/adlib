#pragma once

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "bplusstartree.h"
#include "bplustree.h"
#include "bstartree.h"
#include "btree.h"
#include "random.h"

// #define BSTAR
// #define BPLUS
// #define STRING_MAP

#define ITEMS_PER_NODE 127

#ifdef STRING_MAP
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_MAP(btree, char *, uint32_t, NULL, NULL, ITEMS_PER_NODE, strcmp(a, b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_MAP(btree, char *, uint32_t, NULL, NULL, ITEMS_PER_NODE, strcmp(a, b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_MAP(btree, char *, uint32_t, NULL, NULL, ITEMS_PER_NODE, strcmp(a, b));
# else
DEFINE_BTREE_MAP(btree, char *, uint32_t, NULL, NULL, ITEMS_PER_NODE, strcmp(a, b));
# endif
#else
# if defined(BPLUS) && defined(BSTAR)
DEFINE_BPLUSSTARTREE_SET(btree, int64_t, NULL, ITEMS_PER_NODE, (a < b) ? -1 : (a > b));
# elif defined(BPLUS)
DEFINE_BPLUSTREE_SET(btree, int64_t, NULL, ITEMS_PER_NODE, (a < b) ? -1 : (a > b));
# elif defined(BSTAR)
DEFINE_BSTARTREE_SET(btree, int64_t, NULL, ITEMS_PER_NODE, (a < b) ? -1 : (a > b));
# else
DEFINE_BTREE_SET(btree, int64_t, NULL, ITEMS_PER_NODE, (a < b) ? -1 : (a > b));
# endif
#endif

#if defined(BPLUS) && defined(BSTAR)
typedef struct _bpstartree_node node_type;
typedef struct bplusstartree_info info_type;
typedef struct _bpstartree tree_type;
#define GET_CHILD _bpstartree_debug_node_get_child
#define NODE_SIZE _bpstartree_debug_node_size
#define COPY_TREE _bpstartree_debug_copy
#elif defined(BPLUS)
typedef struct _bplustree_node node_type;
typedef struct bplustree_info info_type;
typedef struct _bplustree tree_type;
#define GET_CHILD _bplustree_debug_node_get_child
#define NODE_SIZE _bplustree_debug_node_size
#define COPY_TREE _bplustree_debug_copy
#elif defined(BSTAR)
typedef struct _bstartree_node node_type;
typedef struct bstartree_info info_type;
typedef struct _bstartree tree_type;
#define GET_CHILD _bstartree_debug_node_get_child
#define NODE_SIZE _bstartree_debug_node_size
#define COPY_TREE _bstartree_debug_copy
#else
typedef struct _btree_node node_type;
typedef struct btree_info info_type;
typedef struct _btree tree_type;
#define GET_CHILD _btree_debug_node_get_child
#define NODE_SIZE _btree_debug_node_size
#define COPY_TREE _btree_debug_copy
#endif

#ifdef STRING_MAP
static char **keys;
static size_t num_keys;
#define __KEY_SIZE 20
#define KEY_SIZE (__KEY_SIZE + 1)
#define KEY_FORMAT __KEY_FORMAT(__KEY_SIZE)
#define __KEY_FORMAT(S) ___KEY_FORMAT(S)
#define ___KEY_FORMAT(S) "%0" #S "zu"

static void init_keys(size_t N)
{
	num_keys = N;
	keys = malloc(N * sizeof(keys[0]));
	for (size_t i = 0; i < N; i++) {
		keys[i] = malloc(KEY_SIZE);
		snprintf(keys[i], KEY_SIZE, KEY_FORMAT, i);
	}
}

static void destroy_keys(void)
{
	for (size_t i = 0; i < num_keys; i++) {
		free(keys[i]);
	}
	free(keys);
	num_keys = 0;
	keys = NULL;
}

static char *get_key(size_t x)
{
	assert(x < num_keys);
	return keys[x];
}

static char **random_keys;
static size_t num_random_keys;

static void init_random_keys(uint32_t *random_numbers, size_t N)
{
	num_random_keys = N;
	random_keys = malloc(N * sizeof(random_keys[0]));
	for (size_t i = 0; i < N; i++) {
		random_keys[i] = malloc(KEY_SIZE);
		snprintf(random_keys[i], KEY_SIZE, KEY_FORMAT, (size_t)random_numbers[i]);
	}
}

static void destroy_random_keys(void)
{
	for (size_t i = 0; i < num_random_keys; i++) {
		free(random_keys[i]);
	}
	free(random_keys);
	num_random_keys = 0;
	random_keys = NULL;
}

static char *get_random_key(size_t i)
{
	assert(i < num_random_keys);
	return random_keys[i];
}
#else
static uint32_t *random_keys;
static size_t num_keys;
static size_t num_random_keys;
static void init_keys(size_t N)
{
	num_keys = N;
}
static void destroy_keys(void)
{
	num_keys = 0;
}
static btree_key_t get_key(size_t x)
{
	assert(x < num_keys);
	return (btree_key_t)x;
}
static void init_random_keys(uint32_t *random_numbers, size_t N)
{
	num_random_keys = N;
	random_keys = random_numbers;
}
static void destroy_random_keys(void)
{
	random_keys = NULL;
	num_random_keys = 0;
}
static btree_key_t get_random_key(size_t i)
{
	assert(i < num_random_keys);
	return (btree_key_t)random_keys[i];
}
#endif

static double ns_elapsed(struct timespec start, struct timespec end)
{
	double s = end.tv_sec - start.tv_sec;
	double ns = end.tv_nsec - start.tv_nsec;
	return ns + 1000000000 * s;
}

static int compare_doubles(const void *_a, const void *_b)
{
	double a = *(const double *)_a;
	double b = *(const double *)_b;
	return (a < b) ? -1 : (a > b);
}

static double get_min(double *values, size_t n)
{
	qsort(values, n, sizeof(values[0]), compare_doubles);
	return values[0];
}

static size_t next_pow2(size_t x)
{
	x--;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	x |= x >> (sizeof(x) * 4);
	x++;
	return x;
}

static size_t btree_node_memory_usage(node_type *node, unsigned int height, bool root, const info_type *info)
{
	size_t size = NODE_SIZE(height == 0, root, info);
	if (height == 0) {
		return size;
	}

	for (unsigned int i = 0; i < node->num_items + 1u; i++) {
		size += btree_node_memory_usage(GET_CHILD(node, i, info), height - 1, false, info);
	}
	return size;
}

static size_t btree_memory_usage(const tree_type *btree, const info_type *info)
{
	node_type *root = btree->root;
	if (btree->height == 0) {
		return 0;
	}
	return btree_node_memory_usage(root, btree->height - 1, true, info);
}

#define DO_NOT_OPTIMIZE(var) asm volatile("" :: "g" (var) : "memory")

int main(void)
{
#ifdef STRING_MAP
	const size_t N = 1 << 16;
#else
	const size_t N = 1 << 18;
#endif
	uint32_t *random_numbers = malloc(2 * N * sizeof(random_numbers[0]));
	uint32_t *random_numbers2 = random_numbers + N;
	{
		struct random_state rng;
		random_state_init(&rng, 0xdeadbeef);
		for (size_t i = 0; i < 2 * N; i++) {
			random_numbers[i] = random_next_u32(&rng);
		}
	}

	init_keys(2 * N);
	init_random_keys(random_numbers, 2 * N);

#define ITERATIONS 15
	double inorder_fast_insert[ITERATIONS];
	double inorder_insert[ITERATIONS];
	double bulk_loading[ITERATIONS];
	double revorder_insert[ITERATIONS];
	double random_insert[ITERATIONS];
	double inorder_find[ITERATIONS];
	double revorder_find[ITERATIONS];
	double randorder_find[ITERATIONS];
	double random_find[ITERATIONS];
	double iteration[ITERATIONS];
	double inorder_deletion[ITERATIONS];
	double revorder_deletion[ITERATIONS];
	double randorder_deletion[ITERATIONS];
	double destruction[ITERATIONS];
	double inorder_mixed[ITERATIONS];
	double revorder_mixed[ITERATIONS];
	double random_mixed[ITERATIONS];

	for (size_t k = 0; k < ITERATIONS; k++) {
		struct btree btree;
		btree_init(&btree);

		struct timespec start_tp, end_tp;

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_key(i);
#ifdef STRING_MAP
			btree_insert(&btree, key, i);
#else
			btree_insert(&btree, key);
#endif
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		inorder_insert[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_key(i);
#ifdef STRING_MAP
			btree_insert_sequential(&btree, key, i);
#else
			btree_insert_sequential(&btree, key);
#endif
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		inorder_fast_insert[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		btree_bulk_load_ctx_t bulk_load_ctx = btree_bulk_load_start();
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_key(i);
#ifdef STRING_MAP
			btree_bulk_load_next(&bulk_load_ctx, key, i);
#else
			btree_bulk_load_next(&bulk_load_ctx, key);
#endif
		}
		btree = btree_bulk_load_end(&bulk_load_ctx);
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		bulk_loading[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			size_t x = N - 1 - i;
			btree_key_t key = get_key(x);
#ifdef STRING_MAP
			btree_insert(&btree, key, x);
#else
			btree_insert(&btree, key);
#endif
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		revorder_insert[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(i);
#ifdef STRING_MAP
			btree_insert(&btree, key, random_numbers[i]);
#else
			btree_insert(&btree, key);
#endif
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		random_insert[k] = ns_elapsed(start_tp, end_tp);


		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(i);
			bool found = btree_find(&btree, key);
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		inorder_find[k] = ns_elapsed(start_tp, end_tp);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(N - 1 - i);
			bool found = btree_find(&btree, key);
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		revorder_find[k] = ns_elapsed(start_tp, end_tp);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(random_numbers2[i] % N);
			bool found = btree_find(&btree, key);
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		randorder_find[k] = ns_elapsed(start_tp, end_tp);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(N + i);
			bool found = btree_find(&btree, key);
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		random_find[k] = ns_elapsed(start_tp, end_tp);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		{
			btree_iter_t iter;
#ifdef STRING_MAP
			btree_key_t prev_key, key;
			btree_value_t *value = btree_iter_start_leftmost(&iter, &btree, &prev_key);
			while ((value = btree_iter_next(&iter, &key))) {
				assert(btree_info.cmp(&prev_key, &key) < 0);
				prev_key = key;
			}
#else
			const btree_key_t *prev_key, *key;
			prev_key = btree_iter_start_leftmost(&iter, &btree);
			while ((key = btree_iter_next(&iter))) {
				assert(btree_info.cmp(prev_key, key) < 0);
				prev_key = key;
			}
#endif
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		iteration[k] = ns_elapsed(start_tp, end_tp);

		struct btree copy;
		copy = (struct btree) {._impl = COPY_TREE(&btree._impl, &btree_info)};

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(i);
#ifdef STRING_MAP
			bool found = btree_delete(&btree, key, &key, NULL);
#else
			bool found = btree_delete(&btree, key, &key);
#endif
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		inorder_deletion[k] = ns_elapsed(start_tp, end_tp);

		btree = copy;
		copy = (struct btree) {._impl = COPY_TREE(&btree._impl, &btree_info)};

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(N - 1 - i);
#ifdef STRING_MAP
			bool found = btree_delete(&btree, key, &key, NULL);
#else
			bool found = btree_delete(&btree, key, &key);
#endif
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		revorder_deletion[k] = ns_elapsed(start_tp, end_tp);

		btree = copy;
		copy = (struct btree) {._impl = COPY_TREE(&btree._impl, &btree_info)};

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N; i++) {
			btree_key_t key = get_random_key(random_numbers2[i] % N);
#ifdef STRING_MAP
			bool found = btree_delete(&btree, key, &key, NULL);
#else
			bool found = btree_delete(&btree, key, &key);
#endif
			DO_NOT_OPTIMIZE(found);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		randorder_deletion[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);
		btree = copy;

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		btree_destroy(&btree);
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		destruction[k] = ns_elapsed(start_tp, end_tp);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N / 4; i++) {
			const size_t INSERTIONS = 5;
			const size_t FINDS = 10;
			const size_t DELETIONS = 2;
			for (size_t j = 0; j < INSERTIONS; j++) {
				size_t z = (i * INSERTIONS + j) % (2 * N);
				btree_key_t key = get_key(z);
#ifdef STRING_MAP
				btree_insert(&btree, key, z);
#else
				btree_insert(&btree, key);
#endif
			}
			size_t n = next_pow2((i + 1) * INSERTIONS);
			if (n > 2 * N) {
				n = 2 * N;
			}
			for (size_t j = 0; j < FINDS; j++) {
				btree_key_t key = get_key((i * FINDS + j) & (n - 1));
				bool found = btree_find(&btree, key);
				DO_NOT_OPTIMIZE(found);
			}
			for (size_t j = 0; j < DELETIONS; j++) {
				btree_key_t key = get_key((i * DELETIONS + 2 * j) & (n - 1));
#ifdef STRING_MAP
				btree_delete(&btree, key, &key, NULL);
#else
				btree_delete(&btree, key, &key);
#endif
			}
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		inorder_mixed[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N / 4; i++) {
			const size_t INSERTIONS = 5;
			const size_t FINDS = 10;
			const size_t DELETIONS = 2;
			for (size_t j = 0; j < INSERTIONS; j++) {
				size_t z = 2 * N - 1 - ((i * INSERTIONS + j) % (2 * N));
				btree_key_t key = get_key(z);
#ifdef STRING_MAP
				btree_insert(&btree, key, z);
#else
				btree_insert(&btree, key);
#endif
			}
			size_t n = next_pow2((i + 1) * INSERTIONS);
			if (n > 2 * N) {
				n = 2 * N;
			}
			for (size_t j = 0; j < FINDS; j++) {
				btree_key_t key = get_key(2 * N - 1 - ((i * FINDS + j) & (n - 1)));
				bool found = btree_find(&btree, key);
				DO_NOT_OPTIMIZE(found);
			}
			for (size_t j = 0; j < DELETIONS; j++) {
				btree_key_t key = get_key(2 * N - 1 - ((i * DELETIONS + 2 * j) & (n - 1)));
#ifdef STRING_MAP
				btree_delete(&btree, key, &key, NULL);
#else
				btree_delete(&btree, key, &key);
#endif
			}
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		revorder_mixed[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);

		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start_tp);
		for (size_t i = 0; i < N / 4; i++) {
			const size_t INSERTIONS = 5;
			const size_t FINDS = 10;
			const size_t DELETIONS = 2;
			for (size_t j = 0; j < INSERTIONS; j++) {
				size_t z = (i * INSERTIONS + j) % (2 * N);
				btree_key_t key = get_random_key(z);
#ifdef STRING_MAP
				btree_insert(&btree, key, random_numbers[z]);
#else
				btree_insert(&btree, key);
#endif
			}
			size_t n = next_pow2((i + 1) * INSERTIONS);
			if (n > 2 * N) {
				n = 2 * N;
			}
			for (size_t j = 0; j < FINDS; j++) {
				btree_key_t key = get_random_key((i * FINDS + j) & (n - 1));
				bool found = btree_find(&btree, key);
				DO_NOT_OPTIMIZE(found);
			}
			for (size_t j = 0; j < DELETIONS; j++) {
				btree_key_t key = get_random_key((i * DELETIONS + 2 * j) & (n - 1));
#ifdef STRING_MAP
				btree_delete(&btree, key, &key, NULL);
#else
				btree_delete(&btree, key, &key);
#endif
			}
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end_tp);
		random_mixed[k] = ns_elapsed(start_tp, end_tp);

		btree_destroy(&btree);
	}

	double t;
	t = get_min(inorder_insert, ITERATIONS);
	printf("%-32s %8.1f ns\n", "in-order insertion", t / N);
	t = get_min(revorder_insert, ITERATIONS);
	printf("%-32s %8.1f ns\n", "reverse-order insertion", t / N);
	t = get_min(random_insert, ITERATIONS);
	printf("%-32s %8.1f ns\n", "random insertion", t / N);
	t = get_min(inorder_fast_insert, ITERATIONS);
	printf("%-32s %8.1f ns\n", "in-order insertion (fastpath)", t / N);
	t = get_min(bulk_loading, ITERATIONS);
	printf("%-32s %8.1f ns\n", "bulk loading", t / N);
	t = get_min(inorder_find, ITERATIONS);
	printf("%-32s %8.1f ns\n", "in-order find", t / N);
	t = get_min(revorder_find, ITERATIONS);
	printf("%-32s %8.1f ns\n", "reverse-order find", t / N);
	t = get_min(randorder_find, ITERATIONS);
	printf("%-32s %8.1f ns\n", "random-order find", t / N);
	t = get_min(random_find, ITERATIONS);
	printf("%-32s %8.1f ns\n", "random find", t / N);
	t = get_min(iteration, ITERATIONS);
	printf("%-32s %8.1f ns\n", "iteration", t / N);
	t = get_min(inorder_deletion, ITERATIONS);
	printf("%-32s %8.1f ns\n", "in-order deletion", t / N);
	t = get_min(revorder_deletion, ITERATIONS);
	printf("%-32s %8.1f ns\n", "reverse-order deletion", t / N);
	t = get_min(randorder_deletion, ITERATIONS);
	printf("%-32s %8.1f ns\n", "random-order deletion", t / N);
	t = get_min(destruction, ITERATIONS);
	printf("%-32s %8.1f ns\n", "destruction", t / N);
	t = get_min(inorder_mixed, ITERATIONS);
	printf("%-32s %8.1f ns\n", "in-order mixed", t / (N / 4));
	t = get_min(revorder_mixed, ITERATIONS);
	printf("%-32s %8.1f ns\n", "reverse-order mixed", t / (N / 4));
	t = get_min(random_mixed, ITERATIONS);
	printf("%-32s %8.1f ns\n", "random-order mixed", t / (N / 4));

	double inorder_memory_efficiency = 0;
	double revorder_memory_efficiency = 0;
	double randorder_memory_efficiency = 0;
	double bulk_loading_memory_efficiency = 0;
	for (size_t n = 1; n <= 5000; n++) {
		size_t key_bytes = n * sizeof(btree_key_t);
		size_t value_bytes = 0;
#ifdef STRING_MAP
		value_bytes = n * sizeof(btree_value_t);
#endif

		struct btree btree;
		btree_init(&btree);

		for (size_t i = 0; i < n; i++) {
			btree_key_t key = get_key(i);
#ifdef STRING_MAP
			btree_insert(&btree, key, i);
#else
			btree_insert(&btree, key);
#endif
		}

		size_t used_bytes = btree_memory_usage(&btree._impl, &btree_info);
		double eff = (double)(key_bytes + value_bytes) / used_bytes;
		inorder_memory_efficiency = (inorder_memory_efficiency * (n - 1) + eff) / n;

		btree_destroy(&btree);

		for (size_t i = 0; i < n; i++) {
			size_t x = n - 1 - i;
			btree_key_t key = get_key(x);
#ifdef STRING_MAP
			btree_insert(&btree, key, x);
#else
			btree_insert(&btree, key);
#endif
		}

		used_bytes = btree_memory_usage(&btree._impl, &btree_info);
		eff = (double)(key_bytes + value_bytes) / used_bytes;
		revorder_memory_efficiency = (revorder_memory_efficiency * (n - 1) + eff) / n;

		btree_destroy(&btree);

		for (size_t i = 0; i < n; i++) {
			btree_key_t key = get_random_key(i);
#ifdef STRING_MAP
			btree_insert(&btree, key, random_numbers[i]);
#else
			btree_insert(&btree, key);
#endif
		}

		used_bytes = btree_memory_usage(&btree._impl, &btree_info);
		eff = (double)(key_bytes + value_bytes) / used_bytes;
		randorder_memory_efficiency = (randorder_memory_efficiency * (n - 1) + eff) / n;

		btree_destroy(&btree);

		btree_bulk_load_ctx_t bulk_load_ctx = btree_bulk_load_start();
		for (size_t i = 0; i < n; i++) {
			btree_key_t key = get_key(i);
#ifdef STRING_MAP
			btree_bulk_load_next(&bulk_load_ctx, key, i);
#else
			btree_bulk_load_next(&bulk_load_ctx, key);
#endif
		}
		btree = btree_bulk_load_end(&bulk_load_ctx);

		used_bytes = btree_memory_usage(&btree._impl, &btree_info);
		eff = (double)(key_bytes + value_bytes) / used_bytes;
		bulk_loading_memory_efficiency = (bulk_loading_memory_efficiency * (n - 1) + eff) / n;

		btree_destroy(&btree);
	}
	printf("%-32s %8.1f %%\n", "in-order memory efficiency", 100 * inorder_memory_efficiency);
	printf("%-32s %8.1f %%\n", "reverse-order memory efficiency", 100 * revorder_memory_efficiency);
	printf("%-32s %8.1f %%\n", "random-order memory efficiency", 100 * randorder_memory_efficiency);
	printf("%-32s %8.1f %%\n", "bulk-loading memory efficiency", 100 * bulk_loading_memory_efficiency);

	destroy_keys();
	destroy_random_keys();
	free(random_numbers);
}
