/*
 * Copyright (C) 2024 Fabian Hügel
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>
#include "config.h"
#include "compiler.h"
#include "uint128.h"

typedef union {
	uint8_t bytes[4];
	uint32_t u32;
} hash32_t;

typedef union {
	uint8_t bytes[8];
	uint64_t u64;
} hash64_t;

typedef union {
	uint8_t bytes[16];
	uint128_t u128;
} hash128_t;

// SipHash-2-4
// (see also https://github.com/veorq/SipHash)
hash64_t siphash24_64(const void *buf, size_t buflen, const unsigned char key[static 16]) _attr_pure;
hash128_t siphash24_128(const void *buf, size_t buflen, const unsigned char key[static 16]) _attr_pure;

// SipHash-1-3
// (see also https://github.com/veorq/SipHash)
hash64_t siphash13_64(const void *buf, size_t buflen, const unsigned char key[static 16]) _attr_pure;
hash128_t siphash13_128(const void *buf, size_t buflen, const unsigned char key[static 16]) _attr_pure;

// mx3 hash
// fast non-cryptographic hash function
// (see also https://github.com/jonmaiga/mx3)
uint64_t mx3_hash(const void *buf, size_t buflen, uint64_t seed) _attr_pure;

uint32_t int_hash32(uint32_t val) _attr_const;
uint64_t int_hash64(uint64_t val) _attr_const;

uint32_t fibonacci_hash32(uint32_t val, unsigned int bits) _attr_const;
uint64_t fibonacci_hash64(uint64_t val, unsigned int bits) _attr_const;

uint32_t pair_hash_int32(uint32_t val1, uint32_t val2) _attr_const;
uint64_t pair_hash_int64(uint64_t val1, uint64_t val2) _attr_const;
