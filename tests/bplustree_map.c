#define BPLUS
#define MAP
#include "btree_test.h"

SIMPLE_TEST(bplustree_map)
{
	return bplustree_map_test();
}

RANDOM_TEST(bplustree_map_random, random_seed, 2)
{
	return bplustree_map_random_test(random_seed);
}

SIMPLE_TEST(bplustree_map_allocated_strings)
{
	return bplustree_map_allocated_strings_test();
}

SIMPLE_TEST(bplustree_map_alignment)
{
	return bplustree_map_alignment_test();
}

SIMPLE_TEST(bplustree_map_bulk_loading)
{
	return bplustree_map_bulk_loading_test();
}
