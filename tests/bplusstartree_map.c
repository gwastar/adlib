#define BPLUS
#define BSTAR
#define MAP
#include "btree_test.h"

SIMPLE_TEST(bplusstartree_map)
{
	return bplusstartree_map_test();
}

RANDOM_TEST(bplusstartree_map_random, random_seed, 2)
{
	return bplusstartree_map_random_test(random_seed);
}

SIMPLE_TEST(bplusstartree_map_allocated_strings)
{
	return bplusstartree_map_allocated_strings_test();
}

SIMPLE_TEST(bplusstartree_map_alignment)
{
	return bplusstartree_map_alignment_test();
}

SIMPLE_TEST(bplusstartree_map_bulk_loading)
{
	return bplusstartree_map_bulk_loading_test();
}
