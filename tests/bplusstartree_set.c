#define BPLUS
#define BSTAR
#include "btree_test.h"

SIMPLE_TEST(bplusstartree_set)
{
	return bplusstartree_set_test();
}

RANDOM_TEST(bplusstartree_set_random, random_seed, 2)
{
	return bplusstartree_set_random_test(random_seed);
}

SIMPLE_TEST(bplusstartree_set_allocated_strings)
{
	return bplusstartree_set_allocated_strings_test();
}

SIMPLE_TEST(bplusstartree_set_alignment)
{
	return bplusstartree_set_alignment_test();
}

SIMPLE_TEST(bplusstartree_set_bulk_loading)
{
	return bplusstartree_set_bulk_loading_test();
}
