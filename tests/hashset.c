#include "hashtable.h"

RANDOM_TEST(hashset_random, random_seed, 64)
{
	return random_test(random_seed);
}

SIMPLE_TEST(hashset_insert)
{
	return insert_test();
}

SIMPLE_TEST(hashset_lookup)
{
	return lookup_test();
}

SIMPLE_TEST(hashset_remove)
{
	return remove_test();
}

SIMPLE_TEST(hashset_insert_unchecked)
{
	return insert_unchecked_test();
}

SIMPLE_TEST(hashset_iterator)
{
	return iterator_test();
}

SIMPLE_TEST(hashset_init_destroy_clear)
{
	return init_destroy_clear_test();
}

SIMPLE_TEST(hashset_resize)
{
	return resize_test();
}

SIMPLE_TEST(hashset_destructors)
{
	return destructors_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify1)
{
	return fortify1_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify2)
{
	return fortify2_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify3)
{
	return fortify3_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify4)
{
	return fortify4_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify5)
{
	return fortify5_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify6)
{
	return fortify6_test();
}

NEGATIVE_SIMPLE_TEST(hashset_fortify7)
{
	return fortify7_test();
}
